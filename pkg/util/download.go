package util

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/PuerkitoBio/goquery"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/pkg/errors"
)

const (
	BaseUrl = "https://adventofcode.com"
)

func GetPuzzleArticleMarkdown(year int, day int) (string, error) {
	html, err := downloadPuzzleArticle(year, day)
	if err != nil {
		return "", err
	}
	return generateMarkdownFromHTML(html)
}

func DownloadPuzzleInput(sessionCookie string, year int, day int) ([]byte, error) {
	inputURL := fmt.Sprintf("%s/%d/day/%d/input", BaseUrl, year, day)

	req, _ := http.NewRequest(http.MethodGet, inputURL, nil)
	req.AddCookie(&http.Cookie{
		Name:  "session",
		Value: sessionCookie,
	})
	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "downloading puzzle input")
	}

	if res.StatusCode != http.StatusOK {
		return nil, errors.Wrap(errors.New(res.Status), "downloading puzzle input")
	}

	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		return nil, errors.Wrap(err, "reading puzzle input")
	}
	return body, nil
}

func downloadPuzzleArticle(year int, day int) ([]byte, error) {
	puzzleURL := fmt.Sprintf("%s/%d/day/%d", BaseUrl, year, day)

	res, err := http.Get(puzzleURL)
	if err != nil {
		return nil, errors.Wrap(err, "downloading puzzle intro")
	}

	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		return nil, errors.Wrap(err, "reading puzzle intro")
	}
	return body, nil
}

func generateMarkdownFromHTML(htmlInput []byte) (string, error) {
	reader := bytes.NewReader(htmlInput)

	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		return "", errors.Wrap(err, "parsing html document")
	}

	article := doc.Find("article")
	if article == nil {
		return "", errors.New("article html element not found")
	}

	converter := md.NewConverter("", true, nil)
	markdown := converter.Convert(article)
	if markdown == "" {
		return "", errors.Wrap(err, "converting article to markdown")
	}

	return addContributionToMarkdown(markdown), nil
}

func addContributionToMarkdown(markdown string) string {
	contributionMD := "*This puzzle was created by [Advent of Code](https://adventofcode.com/)*"
	return fmt.Sprintf("%s/n%s", contributionMD, markdown)
}
