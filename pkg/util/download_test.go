package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDownloadPuzzleIntro(t *testing.T) {
	type args struct {
		year int
		day  int
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{name: "2019/1", args: args{year: 2019, day: 1}, want: "The Tyranny of the Rocket Equation", wantErr: false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := downloadPuzzleArticle(tt.args.year, tt.args.day)
			if (err != nil) != tt.wantErr {
				t.Errorf("DownloadPuzzleIntro() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Contains(t, string(got), tt.want)
		})
	}
}

func Test_parseIntroFromHTML(t *testing.T) {
	html, err := downloadPuzzleArticle(2019, 1)
	assert.NoError(t, err)

	article, err := generateMarkdownFromHTML(html)
	assert.NoError(t, err)
	assert.Contains(t, article, `## \-\-\- Day 1`)
}
