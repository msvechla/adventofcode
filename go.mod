module gitlab.com/msvechla/adventofcode

go 1.15

require (
	github.com/JohannesKaufmann/html-to-markdown v1.2.0
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
