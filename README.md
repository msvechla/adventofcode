# Advent of Code

*My solutions for the [adventofcode.com](https://adventofcode.com) coding puzzles.*

## Usage

Run all tests for a year:

```bash
make test YEAR=2020
```

Run all puzzles with provided input for a year:

```bash
make run YEAR=2020
```

## Solutions

- [2020 in Go](2020/)
- [2024 in Ruby](2024/)

## Acknowledgements

- <https://adventofcode.com>

