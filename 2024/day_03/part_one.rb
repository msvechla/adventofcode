#!/usr/bin/env ruby

# read_input reads the input into a list of memory lines
def read_input
  memoryLines = []
  File.foreach('input.txt') do |line|
    memoryLines << line
  end
  return memoryLines
end

# extractMulInstructions returns a list of number pairs
def extractMulInstructions(line)
  return line.scan(/mul\((\d+),(\d+)\)/)
end

def extract(inputLines)
  instructions = []
  inputLines.each do |l|
    i = extractMulInstructions(l)
    instructions.append(*i)
  end
  return instructions
end

def calcMulSum(instructions)
  sum = 0
  instructions.each do |i|
    sum += (i[0].to_i * i[1].to_i)
  end
  return sum
end

if __FILE__ == $0
  memoryLines = read_input
  instructions = extract(memoryLines)
  sum = calcMulSum(instructions)

  puts "Sum: #{sum}"
end
