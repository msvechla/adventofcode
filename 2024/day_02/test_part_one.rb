#!/usr/bin/env ruby

require 'test/unit'
require_relative 'part_one'

class TestPartOne < Test::Unit::TestCase
  def test_is_safe_report
    # Test case 1: Safe because the levels are all decreasing by 1 or 2
    assert_true(is_safe_report([7, 6, 4, 2, 1]), "7 6 4 2 1 should be safe")

    # Test case 2: Unsafe because 2 7 is an increase of 5
    assert_false(is_safe_report([1, 2, 7, 8, 9]), "1 2 7 8 9 should be unsafe")

    # Test case 3: Unsafe because 6 2 is a decrease of 4
    assert_false(is_safe_report([9, 7, 6, 2, 1]), "9 7 6 2 1 should be unsafe")

    # Test case 4: Unsafe because 1 3 is increasing but 3 2 is decreasing
    assert_false(is_safe_report([1, 3, 2, 4, 5]), "1 3 2 4 5 should be unsafe")

    # Test case 5: Unsafe because 4 4 is neither an increase or a decrease
    assert_false(is_safe_report([8, 6, 4, 4, 1]), "8 6 4 4 1 should be unsafe")

    # Test case 6: Safe because the levels are all increasing by 1, 2, or 3
    assert_true(is_safe_report([1, 3, 6, 7, 9]), "1 3 6 7 9 should be safe")

    # Additional test cases
    assert_true(is_safe_report([]), "Empty array should be safe")
    assert_true(is_safe_report([5]), "Single element array should be safe")
  end
end
