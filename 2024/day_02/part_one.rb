#!/usr/bin/env ruby

# read_input reads the input into a list of reports
def read_input
  reports = []
  File.foreach('input.txt') do |line|
    reports << line.split.map(&:to_i)
  end
  reports
end

def is_safe_report(report)
  return true if report.size <= 1
  
  # The levels are either all increasing or all decreasing
  direction = report[1] <=> report[0]
  report.each_cons(2) do |prev, curr|
    return false if prev == curr
    curr_direction = curr <=> prev
    return false if curr_direction != 0 && curr_direction != direction
    
    # Any two adjacent levels differ by at least one and at most three
    diff = (prev - curr).abs
    return false if diff < 1 || diff > 3

  end
  true
end

def count_safe_reports(reports)
  reports.count { |r| is_safe_report(r) }
end

if __FILE__ == $0
  reports = read_input
  safeReports = count_safe_reports(reports)

  puts "Safe Reports: #{safeReports}"
end
