#!/usr/bin/env ruby

def read_input
  reports = []
  File.foreach('input.txt') do |line|
    reports << line.split.map(&:to_i)
  end
  reports
end

def is_safe_seq(direction, prev, curr)
    return false if prev == curr
    curr_direction = prev <=> curr
    return false if curr_direction != 0 && curr_direction != direction
    
    diff = (prev - curr).abs
    return false if diff < 1 || diff > 3
    return true
end

def is_safe_report(report)
  return true if report.size <= 1
  
  # skipIndex is the index that should be skipped
  skipIndex = -1

  while skipIndex < report.length
    isSafe = false

    indicesToCompare = [*0.. (report.length - 1) ].select { |i| i != skipIndex } 

    direction = report[indicesToCompare[0]] <=> report[indicesToCompare[1]]

    indicesToCompare.each_cons(2) do |prevIndex, currIndex|
      prev = report[prevIndex]
      curr = report[currIndex]
      isSafe = is_safe_seq(direction, prev, curr)
      break if not isSafe
    end

    skipIndex += 1

    return true if isSafe
  end
  return false
end

def count_safe_reports(reports)
  reports.count { |r| is_safe_report(r) }
end

if __FILE__ == $0
  reports = read_input
  safeReports = count_safe_reports(reports)

  puts "Safe Reports: #{safeReports}"
end
