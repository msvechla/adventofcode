#!/usr/bin/env ruby

require 'test/unit'
require_relative 'part_two'

class TestPartTwo < Test::Unit::TestCase
  def test_is_safe_report
    # Test case 1: Safe without removing any level
    assert_true(is_safe_report([7, 6, 4, 2, 1]), "7 6 4 2 1 should be safe without removing any level")

    # Test case 2: Unsafe regardless of which level is removed
    assert_false(is_safe_report([1, 2, 7, 8, 9]), "1 2 7 8 9 should be unsafe regardless of which level is removed")

    # Test case 3: Unsafe regardless of which level is removed
    assert_false(is_safe_report([9, 7, 6, 2, 1]), "9 7 6 2 1 should be unsafe regardless of which level is removed")

    # Test case 4: Safe by removing the second level, 3
    assert_true(is_safe_report([1, 3, 2, 4, 5]), "1 3 2 4 5 should be safe by removing the second level, 3")

    # Test case 5: Safe by removing the third level, 4
    assert_true(is_safe_report([8, 6, 4, 4, 1]), "8 6 4 4 1 should be safe by removing the third level, 4")

    # Test case 6: Safe without removing any level
    assert_true(is_safe_report([1, 3, 6, 7, 9]), "1 3 6 7 9 should be safe without removing any level")

    # Additional edge cases
    assert_true(is_safe_report([]), "Empty array should be safe")
    assert_true(is_safe_report([5]), "Single element array should be safe")
  end
end
