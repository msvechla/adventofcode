#!/usr/bin/env ruby

require 'test/unit'
require_relative 'part_one'

class TestPartOne < Test::Unit::TestCase
  def test_has_xmas_horizontal
    # Test case 1: XMAS in order
    assert_true(has_xmas_horizontal(['X', 'M', 'A', 'S'], 1, 0), "XMAS in order should return true")

    # Test case 2: XMAS in reverse order
    assert_true(has_xmas_horizontal(['S', 'A', 'M', 'X'], -1, 3), "XMAS in reverse order should return true")

    # Test case 3: XMAS with spacing
    assert_true(has_xmas_horizontal(['X', 'a', 'M', 'b', 'A', 'c', 'S'], 2, 0), "XMAS with spacing should return true")

    # Test case 4: Incomplete XMAS
    assert_false(has_xmas_horizontal(['X', 'M', 'A'], 1, 0), "Incomplete XMAS should return false")

    # Test case 5: No XMAS
    assert_false(has_xmas_horizontal(['A', 'B', 'C', 'D'], 1, 0), "No XMAS should return false")

    # Test case 6: XMAS out of bounds (positive increment)
    assert_false(has_xmas_horizontal(['X', 'M', 'A'], 1, 1), "XMAS out of bounds (positive increment) should return false")

    # Test case 7: XMAS out of bounds (negative increment)
    assert_false(has_xmas_horizontal(['X', 'M', 'A', 'S'], -1, 1), "XMAS out of bounds (negative increment) should return false")

    # Test case 8: XMAS in larger array
    assert_true(has_xmas_horizontal(['A', 'B', 'X', 'M', 'A', 'S', 'C', 'D'], 1, 2), "XMAS in larger array should return true")
  end

  def setup
    @test_array_vertical = [
      ['M', 'A', 'S', 'C'],
      ['X', 'A', 'S', 'C'],
      ['M', 'D', 'A', 'F'],
      ['A', 'G', 'M', 'I'],
      ['S', 'J', 'X', 'L']
    ]
    
    @test_array_diagonal = [
      ['X', 'A', 'S', 'C', 'C'],
      ['X', 'M', 'S', 'S', 'C'],
      ['M', 'D', 'A', 'F', 'F'],
      ['A', 'M', 'M', 'S', 'S'],
      ['X', 'J', 'M', 'L', 'L'],
      ['S', 'X', 'X', 'L', 'L']
    ]
  end

  def test_has_xmas_vertical
    # Test positive case (downward)
    assert_true(has_xmas_vertical(@test_array_vertical, 1, 0, 1), "Should find XMAS vertically downward")

    # Test positive case (upward)
    assert_true(has_xmas_vertical(@test_array_vertical, -1, 2, 4), "Should find XMAS vertically upward")

    # Test negative case (no XMAS)
    assert_false(has_xmas_vertical(@test_array_vertical, 1, 1, 0), "Should not find XMAS starting from second column")

    # Test edge case (out of bounds downward)
    assert_false(has_xmas_vertical(@test_array_vertical, 1, 1, 1), "Should return false when going out of bounds downward")

    # Test edge case (out of bounds upward)
    assert_false(has_xmas_vertical(@test_array_vertical, -1, 0, 2), "Should return false when going out of bounds upward")

    # Test with different increment
    assert_false(has_xmas_vertical(@test_array_vertical, 2, 0, 0), "Should return false with increment 2")
  end
  
  def test_has_xmas_diagnoal
    # Test positive case (diagnoal downward)
    assert_true(has_xmas_diagonal(@test_array_diagonal, 1, 1, 0, 0), "Should find XMAS diagonally downward")

    # Test positive case (diagonal upward)
    assert_true(has_xmas_diagonal(@test_array_diagonal, 1, -1, 0, 4), "Should find XMAS diagonally upward")
  end
end
