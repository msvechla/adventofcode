#!/usr/bin/env ruby

XMASChars = "XMAS".split("")

# read_input reads the input into a 2 dimensional array of characters
def read_input
  xmas = []
  File.foreach('input.txt') do |line|
    xmas << line.chomp("\n").split("")
  end
  xmas
end

def has_xmas_horizontal(arr, inc, startIndex)
  endIndex = startIndex + (inc * XMASChars.length) - (inc)
  if endIndex < 0 || endIndex > arr.length - 1
    return false
  end

  xmasIndex = 0
  startIndex.step(endIndex, inc) do |i|
    if arr[i] != XMASChars[xmasIndex]
      return false
    end
    xmasIndex+=1
  end
  return true
end

def has_xmas_vertical(arr, inc, startX, startY)
  endY= startY + (inc * XMASChars.length) - (inc)
  if endY< 0 || endY> arr.length - 1
    return false
  end
  
  xmasIndex = 0
  startY.step(endY, inc) do |y|
    if arr[y][startX] != XMASChars[xmasIndex]
      return false
    end
    xmasIndex+=1
  end
  return true
end

def has_xmas_diagonal(arr, incX, incY, startX, startY)
  endX= startX + (incX * XMASChars.length) - (incX)
  endY= startY + (incY * XMASChars.length) - (incY)

  if endY< 0 || endY> arr.length - 1
    return false
  end
  if endX< 0 || endX> arr[startY].length - 1
    return false
  end
  
  xmasIndex = 0
  x = startX
  y = startY

  for _ in [*0..XMASChars.length - 1] do
      if arr[y][x] != XMASChars[xmasIndex]
        return false
      end
      xmasIndex+=1
      x += incX
      y += incY
  end

  return true
end

# find_xmas finds all occurences of X and then checks for xmas
def find_xmas(arr)

  occ = 0
  arr.each_with_index do |line, y|
    line.each_with_index do |char, x|
      if char == "X"
        occ += 1 if has_xmas_horizontal(line, 1, x)
        occ += 1 if has_xmas_horizontal(line, -1, x)
        occ += 1 if has_xmas_vertical(arr, 1, x, y)
        occ += 1 if has_xmas_vertical(arr, -1, x, y)
        occ += 1 if has_xmas_diagonal(arr, 1, 1, x, y)
        occ += 1 if has_xmas_diagonal(arr, 1, -1, x, y)
        occ += 1 if has_xmas_diagonal(arr, -1, 1, x, y)
        occ += 1 if has_xmas_diagonal(arr, -1, -1, x, y)
      end
    end
  end

  return occ
end

if __FILE__ == $0
  xmasInput = read_input

  occ = find_xmas(xmasInput)

  puts "XMAS: #{occ}"
end
