#!/usr/bin/env ruby

require 'test/unit'
require_relative 'part_one'

class TestPartOne < Test::Unit::TestCase
  def setup
    @rules = [
      [47, 53], [97, 13], [97, 61], [97, 47], [75, 29],
      [61, 13], [75, 53], [29, 13], [97, 29], [53, 29],
      [61, 53], [97, 53], [61, 29], [47, 13], [75, 47],
      [97, 75], [47, 61], [75, 61], [47, 29], [75, 13],
      [53, 13]
    ]
  end

  def test_is_valid_update
    # Test the given update
    assert_true(is_valid_update([75, 47, 61, 53, 29], @rules), "Update [75, 47, 61, 53, 29] should be valid")
    assert_true(is_valid_update([97,61,53,29,13], @rules), "Update [97,61,53,29,13] should be valid")
    assert_true(is_valid_update([75,29,13], @rules), "Update [75,29,13] should be valid")

    # Test invalid updates
    assert_false(is_valid_update([53, 47], @rules), "Update [53, 47] should be invalid due to rule [47, 53]")

    assert_false(is_valid_update([13, 97], @rules), "Update [13, 97] should be invalid due to rule [97, 13]")
    assert_false(is_valid_update([29, 75, 47], @rules), "Update [29, 75, 47] should be invalid due to rule [75, 29]")
    assert_false(is_valid_update([75,97,47,61,53], @rules), "75,97,47,61,53, is not in the correct order: it would print 75 before 97, which violates the rule 97|75.")

  end
end
