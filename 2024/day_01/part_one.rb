#!/usr/bin/env ruby

# read_input_sort reads the inputs and sorts them in ascending order in two lists
def read_input_sort

  a, b = [],[]

  File.foreach('input.txt') do |line|
    locationIDs = line.split
    newA = locationIDs[0].to_i
    newB = locationIDs[1].to_i

    if a.length == 0
      a[0] = newA
      b[0] = newB
      next
    end

    insertedA = false
    a.each_with_index do |num, i|
      if newA <= num
        a.insert(i, newA)
        insertedA = true
        break
      end
    end
    if not insertedA
      a.append(newA)
    end
    
    insertedB = false
    b.each_with_index do |num, i|
      if newB <= num
        b.insert(i, newB)
        insertedB = true
        break
      end
    end
    if not insertedB
      b.append(newB)
    end

  end

  return a,b
end

def sum_of_diffs(a,b)
  sum = 0
  a.each_with_index do |numA, i|
    diff = (numA - b[i]).abs
    puts diff
    sum = sum + diff
  end
  return sum
end

if __FILE__ == $0
  a, b = read_input_sort

  puts "List A (#{a.length}) #{a}"
  puts "List B (#{b.length}) #{b}"
  puts sum_of_diffs(a,b)
end

