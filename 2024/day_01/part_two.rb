#!/usr/bin/env ruby

# read_input_map reads the first column into a list and the second column into a map
def read_input_map

  a = []
  b = {}

  File.foreach('input.txt') do |line|
    locationIDs = line.split
    newA = locationIDs[0].to_i
    newB = locationIDs[1].to_i

    a.append(newA)
    b[newB] = b.fetch(newB, 0) +1
  end

  return a,b
end

def similarity_score(a,b)
  score = 0
  a.each do |numA|
    score += numA * b.fetch(numA, 0)
  end
  return score
end

if __FILE__ == $0
  a, b = read_input_map
  puts similarity_score(a,b)
end
