# Advent of Code 2019 - Day 1

- Puzzle: <https://adventofcode.com/2019/day/1>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

