package main

import (
	"testing"
)

func Test_calcFuel(t *testing.T) {
	type args struct {
		mass int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "1", args: args{mass: 12}, want: 2},
		{name: "2", args: args{mass: 14}, want: 2},
		{name: "3", args: args{mass: 1969}, want: 654},
		{name: "3", args: args{mass: 100756}, want: 33583},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calcFuel(tt.args.mass); got != tt.want {
				t.Errorf("calcFuel() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sumFuelTotal(t *testing.T) {
	type args struct {
		masses []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "1", args: args{masses: []string{"14"}}, want: 2},
		{name: "2", args: args{masses: []string{"1969"}}, want: 966},
		{name: "2", args: args{masses: []string{"100756"}}, want: 50346},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sumFuelTotal(tt.args.masses); got != tt.want {
				t.Errorf("sumFuelTotal() = %v, want %v", got, tt.want)
			}
		})
	}
}
