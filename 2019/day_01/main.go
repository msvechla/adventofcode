package main

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {

	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	mLines := strings.Split(string(input), "\n")
	println(sumFuel(mLines))
	println(sumFuelTotal(mLines))
}

func sumFuel(masses []string) int {
	fuelSum := 0

	for _, m := range masses {
		mass, err := strconv.Atoi(m)
		if err != nil {
			continue
		}

		fuelSum += calcFuel(mass)
	}
	return fuelSum
}

func calcFuel(mass int) int {
	return (mass / 3) - 2
}

func calcFuelTotal(masses []string) int {
	if len(masses) == 0 {
		return 0
	}

	mass, err := strconv.Atoi(masses[0])
	if err != nil {
		return 0
	}

	currentFuel := calcFuel(mass)
	if currentFuel < 0 {
		currentFuel = 0
	}

	newMasses := []string{}
	if len(masses) > 1 {
		newMasses = masses[1:]
	}
	if currentFuel > 0 {
		newMasses = append(newMasses, strconv.Itoa(currentFuel))
	}

	return currentFuel + sumFuelTotal(newMasses)
}

func sumFuelTotal(masses []string) int {
	fuelSum := 0

	for _, m := range masses {
		fuelSum += calcFuelTotal([]string{m})
	}
	return fuelSum
}
