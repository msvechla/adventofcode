# Year prefix to act on
YEAR?=20
DAY?=*
COVERPROFILE?=/tmp/cover.out

test:
	go test ./$(YEAR)... -coverprofile $(COVERPROFILE)

run:
	find . -name 'main.go' -path '*/$(YEAR)*/day_$(DAY)' -execdir go run . \;
