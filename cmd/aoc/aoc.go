package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/msvechla/adventofcode/pkg/util"
)

const (
	inputFileName = "input.txt"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:  "day",
				Usage: "advent day to access",
			},
			&cli.IntFlag{
				Name:  "year",
				Usage: "advent year to access",
			},
			&cli.StringFlag{
				Name:     "session_cookie",
				EnvVars:  []string{"SESSION_COOKIE"},
				Required: true,
				Usage:    "session cookie to authenticate to adventofcode.com",
			},
			&cli.StringFlag{
				Name:        "out",
				DefaultText: "%d/day_%02d",
				Usage:       "the output directory format string which will get the year and day passed",
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "download",
				Usage: "download the advent of code input for a given year and day",
				Action: func(c *cli.Context) error {
					return download(c.Int("year"), c.Int("day"), c.String("out"), c.String("session_cookie"))
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func download(year int, day int, out string, sessionCookie string) error {
	if day == 0 {
		day = time.Now().Day()
	}
	if year == 0 {
		year = time.Now().Year()
	}

	downloadedInput, err := util.DownloadPuzzleInput(sessionCookie, year, day)
	if err != nil {
		return errors.Wrapf(err, "downloading puzzle input for year %d and day %d", year, day)
	}

	if out == "" {
		// output to stdout if no output is specified
		_, err = fmt.Fprintln(os.Stdout, string(downloadedInput))
		return err
	}

	outputDir := fmt.Sprintf(out, year, day)

	err = os.MkdirAll(outputDir, 0777)
	if err != nil {
		return errors.Wrap(err, "creating output directory")
	}

	err = ioutil.WriteFile(filepath.Join(outputDir, inputFileName), downloadedInput, 0777)
	if err != nil {
		return errors.Wrap(err, "writing input file")
	}

	return nil
}
