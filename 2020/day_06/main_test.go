package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_group_numUniqueAnsers(t *testing.T) {
	tests := []struct {
		name string
		g    group
		want int
	}{
		{name: "1", g: []answer{[]rune{'a', 'b', 'c'}}, want: 3},
		{name: "2", g: []answer{[]rune{'a'}, []rune{'b'}, []rune{'c'}}, want: 3},
		{name: "3", g: []answer{[]rune{'a'}, []rune{'a'}, []rune{'c'}}, want: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.g.numUniqueAnsers(); got != tt.want {
				t.Errorf("group.numUniqueAnsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_group_numUnanimousAnsers(t *testing.T) {
	tests := []struct {
		name string
		g    group
		want int
	}{
		{name: "1", g: []answer{[]rune{'a', 'b', 'c'}}, want: 3},
		{name: "2", g: []answer{[]rune{'a'}, []rune{'b'}, []rune{'c'}}, want: 0},
		{name: "3", g: []answer{[]rune{'a'}, []rune{'a'}, []rune{'c'}}, want: 0},
		{name: "3", g: []answer{[]rune{'a'}, []rune{'a'}, []rune{'a', 'c'}}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.g.numUnanimousAnsers(); got != tt.want {
				t.Errorf("group.numUnanimousAnsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseGroups(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.ElementsMatch(t, parseGroups()[2], []answer{[]rune{'g', 'd'}, []rune{'p', 'f', 'd', 'y'}})
		})
	}
}
