package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type answer []rune
type group []answer

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 06: Custom Customs")

	groups := parseGroups()
	uniqueAnswersCount := 0
	unanimousAnswersCount := 0
	for _, g := range groups {
		uniqueAnswersCount += g.numUniqueAnsers()
		unanimousAnswersCount += g.numUnanimousAnsers()
	}

	fmt.Printf("Part1: %d (sum of uniquely answered questions in groups)\n", uniqueAnswersCount)
	fmt.Printf("Part2: %d (sum of unanimous answered questions in groups)\n", unanimousAnswersCount)

}

func parseGroups() []group {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")

	groups := make([]group, len(lines)-1)
	countGroups := 0
	currentGroup := group{}

	for _, l := range lines {
		if l == "" {
			groups[countGroups] = currentGroup
			currentGroup = group{}
			countGroups++
			continue
		}
		currentGroup = append(currentGroup, []rune(l))
	}
	return groups[:countGroups]
}

func (g group) numUniqueAnsers() int {
	return len(g.uniqueAnswers())
}

func (g group) uniqueAnswers() map[rune]bool {
	answers := make(map[rune]bool, len(g[0]))

	for _, aSlice := range g {
		for _, a := range aSlice {
			answers[rune(a)] = true
		}
	}
	return answers
}

func (g group) numUnanimousAnsers() int {
	return len(g.unanimousAnswers())
}

func (g group) unanimousAnswers() map[rune]bool {
	answers := make(map[rune]int, len(g[0]))
	unanimousAnswers := map[rune]bool{}
	groupSize := len(g)

	for _, aSlice := range g {
		for _, a := range aSlice {
			answers[rune(a)]++
			if answers[rune(a)] == groupSize {
				unanimousAnswers[rune(a)] = true
			}
		}
	}
	return unanimousAnswers
}

func (a answer) String() string {
	return string(a)
}
