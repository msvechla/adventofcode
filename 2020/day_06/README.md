# Advent of Code 2020 - Day 6: Custom Customs

- Puzzle: <https://adventofcode.com/2020/day/6>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

