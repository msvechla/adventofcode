# Advent of Code 2020 - Day 8: Handheld Halting

- Puzzle: <https://adventofcode.com/2020/day/8>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

