package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type instruction struct {
	op  string
	arg int
}

type bootcode struct {
	instructions []instruction
	values       map[string]int
	visisted     map[int]bool
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 08: Handheld Halting")
	instructions := parseInstructionsFromFile()
	bc := bootcode{
		instructions: instructions,
		values:       map[string]int{},
		visisted:     map[int]bool{},
	}
	_, acc := bc.detectLoopAndReturnAcc(0)

	fmt.Printf("Part1: %v (value of acc just before running next loop)\n", acc)

	bcNoLoop := bootcode{
		instructions: instructions,
		values:       map[string]int{},
		visisted:     map[int]bool{},
	}
	accNoLoop := bcNoLoop.removeLoopAndReturnAcc()
	fmt.Printf("Part2: %v (value of acc after running last instruction without loop)\n", accNoLoop)
}

func (b *bootcode) removeLoopAndReturnAcc() int {
	for i, currentInstruction := range b.instructions {
		newB := *b
		switch currentInstruction.op {
		case "nop":
			newB.instructions = make([]instruction, len(b.instructions))
			copy(newB.instructions, b.instructions)
			newB.instructions[i] = instruction{op: "jmp", arg: currentInstruction.arg}
		case "jmp":
			newB.instructions = make([]instruction, len(b.instructions))
			copy(newB.instructions, b.instructions)
			newB.instructions[i] = instruction{op: "nop", arg: currentInstruction.arg}
		default:
			continue
		}
		newB.values = map[string]int{}
		newB.visisted = map[int]bool{}

		loop, acc := newB.detectLoopAndReturnAcc(0)
		if !loop {
			return acc
		}
	}
	return 0
}

func (b *bootcode) detectLoopAndReturnAcc(pos int) (bool, int) {
	if pos == len(b.instructions) {
		return false, b.values["acc"]
	}

	if _, visited := b.visisted[pos]; visited {
		return true, b.values["acc"]
	}
	b.visisted[pos] = true

	currentInstruction := b.instructions[pos]

	switch currentInstruction.op {
	case "nop":
		return b.detectLoopAndReturnAcc(pos + 1)
	case "acc":
		b.values["acc"] += currentInstruction.arg
		return b.detectLoopAndReturnAcc(pos + 1)
	case "jmp":
		return b.detectLoopAndReturnAcc(pos + currentInstruction.arg)
	}

	return true, 0
}

func parseInstructionsFromFile() []instruction {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInstructions(input)
}

func parseInstructions(input []byte) []instruction {

	lines := strings.Split(string(input), "\n")
	instructions := make([]instruction, len(lines)-1)

	iCount := 0
	for _, l := range lines {
		if l == "" {
			continue
		}

		kv := strings.Split(l, " ")
		arg, _ := strconv.Atoi(kv[1])
		instructions[iCount] = instruction{op: kv[0], arg: arg}
		iCount++
	}
	return instructions
}
