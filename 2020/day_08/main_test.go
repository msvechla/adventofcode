package main

import (
	"reflect"
	"testing"
)

func Test_parseInstructions(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []instruction
	}{
		{name: "example", args: args{input: []byte(`
nop +0
acc +1`)}, want: []instruction{instruction{op: "nop", arg: 0}, instruction{op: "acc", arg: 1}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseInstructions(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseInstructions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_bootcode_detectLoopAndReturnAcc(t *testing.T) {
	type fields struct {
		instructions []instruction
		values       map[string]int
		visisted     map[int]bool
	}
	type args struct {
		pos int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{name: "example", args: args{pos: 0}, fields: fields{instructions: parseInstructions([]byte(`
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`)), values: map[string]int{}, visisted: map[int]bool{}}, want: 5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &bootcode{
				instructions: tt.fields.instructions,
				values:       tt.fields.values,
				visisted:     tt.fields.visisted,
			}
			if _, got := b.detectLoopAndReturnAcc(tt.args.pos); got != tt.want {
				t.Errorf("bootcode.detectLoopAndReturnAcc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_bootcode_removeLoopAndReturnAcc(t *testing.T) {
	type fields struct {
		instructions []instruction
		values       map[string]int
		visisted     map[int]bool
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{name: "example", fields: fields{instructions: parseInstructions([]byte(`
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`)), values: map[string]int{}, visisted: map[int]bool{}}, want: 8},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &bootcode{
				instructions: tt.fields.instructions,
				values:       tt.fields.values,
				visisted:     tt.fields.visisted,
			}
			if got := b.removeLoopAndReturnAcc(); got != tt.want {
				t.Errorf("bootcode.removeLoopAndReturnAcc() = %v, want %v", got, tt.want)
			}
		})
	}
}
