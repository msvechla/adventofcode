package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_boardingpass_getRow(t *testing.T) {
	type args struct {
		currentPos int
		min        int
		max        int
	}
	tests := []struct {
		name string
		b    boardingpass
		args args
		want int
	}{
		{name: "1", args: args{currentPos: 0, min: 0, max: maxRow}, b: []rune("FBFBBFFRLR"), want: 44},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.getRow(tt.args.currentPos, tt.args.min, tt.args.max); got != tt.want {
				t.Errorf("boardingpass.getRow() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_boardingpass_getCol(t *testing.T) {
	type args struct {
		currentPos int
		min        int
		max        int
	}
	tests := []struct {
		name string
		b    boardingpass
		args args
		want int
	}{
		{name: "1", args: args{currentPos: colStart, min: 0, max: maxCols}, b: []rune("FBFBBFFRLR"), want: 5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.getCol(tt.args.currentPos, tt.args.min, tt.args.max); got != tt.want {
				t.Errorf("boardingpass.getCol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_boardingpass_Row(t *testing.T) {
	tests := []struct {
		name string
		b    boardingpass
		want int
	}{
		{name: "1", b: []rune("FBFBBFFRLR"), want: 44},
		{name: "2", b: []rune("BFFFBBFRRR"), want: 70},
		{name: "3", b: []rune("FFFBBBFRRR"), want: 14},
		{name: "4", b: []rune("BBFFBBFRLL"), want: 102},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.Row(); got != tt.want {
				t.Errorf("boardingpass.Row() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_boardingpass_Col(t *testing.T) {
	tests := []struct {
		name string
		b    boardingpass
		want int
	}{
		// TODO: Add test cases.
		{name: "1", b: []rune("FBFBBFFRLR"), want: 5},
		{name: "2", b: []rune("BFFFBBFRRR"), want: 7},
		{name: "3", b: []rune("FFFBBBFRRR"), want: 7},
		{name: "3", b: []rune("BBFFBBFRLL"), want: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.Col(); got != tt.want {
				t.Errorf("boardingpass.Col() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_boardingpass_SeatID(t *testing.T) {
	tests := []struct {
		name string
		b    boardingpass
		want int
	}{
		{name: "1", b: []rune("FBFBBFFRLR"), want: 357},
		{name: "2", b: []rune("BFFFBBFRRR"), want: 567},
		{name: "3", b: []rune("FFFBBBFRRR"), want: 119},
		{name: "4", b: []rune("BBFFBBFRLL"), want: 820},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.SeatID(); got != tt.want {
				t.Errorf("boardingpass.SeatID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseBoardingpasses(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, string(parseBoardingpasses()[0]), "BFFFBBFRLR")
		})
	}
}
