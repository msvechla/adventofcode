# Advent of Code 2020 - Day 5: Binary Boarding

- Puzzle: <https://adventofcode.com/2020/day/5>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

