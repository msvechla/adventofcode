package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strings"
)

type boardingpass []rune

const (
	maxRow   = 127
	maxCols  = 7
	rowStart = 0
	colStart = 7
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 05: Binary Boarding")

	passes := parseBoardingpasses()
	passIDs := make(map[int]bool, len(passes))

	// part 1
	highestSeatID := 0
	lowestSeatID := 1000
	for _, p := range passes {
		sID := p.SeatID()
		passIDs[sID] = true

		if sID > highestSeatID {
			highestSeatID = sID
		}
		if sID < lowestSeatID {
			lowestSeatID = sID
		}
	}
	fmt.Printf("Part1: %d (the highest SeatID)\n", highestSeatID)

	// part 2
	myID := 0

	for id := lowestSeatID; id < highestSeatID; id++ {
		if _, exists := passIDs[id]; !exists {
			myID = id
			break
		}
	}
	fmt.Printf("Part2: %d (your SeatID)\n", myID)
}

func parseBoardingpasses() []boardingpass {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")

	boardingpasses := make([]boardingpass, len(lines)-1)
	for i, l := range lines {
		if l == "" {
			continue
		}
		boardingpasses[i] = boardingpass(l)
	}
	return boardingpasses
}

func (b boardingpass) SeatID() int {
	return b.Row()*8 + b.Col()
}

func (b boardingpass) Row() int {
	return b.getRow(0, 0, maxRow)
}

func (b boardingpass) Col() int {
	return b.getCol(colStart, 0, maxCols)
}

func (b boardingpass) getRow(currentPos, min, max int) int {
	if currentPos == colStart-1 {
		return getNewRangeByChar(b[6], min, max)[0]
	}
	newRange := getNewRangeByChar(b[currentPos], min, max)
	return b.getRow(currentPos+1, newRange[0], newRange[1])
}

func (b boardingpass) getCol(currentPos, min, max int) int {
	if currentPos == 9 {
		return getNewRangeByChar(b[9], min, max)[0]
	}
	newRange := getNewRangeByChar(b[currentPos], min, max)
	return b.getCol(currentPos+1, newRange[0], newRange[1])
}

func getNewRangeByChar(c rune, min, max int) []int {
	switch c {
	case 'F', 'L':
		if max-min == 1 {
			return []int{min}
		}
		return []int{min, min + (max-min)/2}
	case 'B', 'R':
		if max-min == 1 {
			return []int{max}
		}
		return []int{min + int(math.Ceil(float64(max-min)/2.0)), max}
	}
	return []int{}
}
