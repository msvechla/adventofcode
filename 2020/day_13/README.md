# Advent of Code 2020 - Day 13: Shuttle Search

- Puzzle: <https://adventofcode.com/2020/day/13>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

## Solution Approach for Part 2

- find matching timeslot for previoulsy matched busIDs and next busID in the schedule

This can be done by finding the multiplier `x` for which the following holds true:

```go
(lastTSMatch + x * LCMPrime(previouslyMatchedBusIDs) + departureDiff(currentBusID)) % currentBusID == 0
```

*With `LCMPrime` being the lowest common multiplier for a given set of prime numbers*. 

- this is repeated until all busIDs in the schedule have been matched

