package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

type schedule struct {
	times             []int
	earliestDeparture int
}

type scheduleConstraint struct {
	busID         int
	departureDiff int
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 13: Shuttle Search")
	s := parseScheduleFromFile()
	busID, ed := s.getEarliestBusIDAndDepartureTime()
	fmt.Printf("Part1: %d (ID earliest bus multiplied by the number of minutes to wait)\n", (ed-s.earliestDeparture)*busID)

	constraints := parseScheduleConstraintsFromFile()
	t := findTimestampMatchingDepartureTimes(0, 0, constraints)
	fmt.Printf("Part2: %d (earliest timestamp such that all of the listed bus IDs depart at offsets matching their positions in the list)\n", t)
}

func findTimestampMatchingDepartureTimes(currBusSlot int, lastTSMatch int, constraints []scheduleConstraint) int {
	increments := constraints[0].busID
	if currBusSlot-1 > 0 {
		matchedBusIDs := make([]int, currBusSlot)
		for i, c := range constraints[:currBusSlot] {
			matchedBusIDs[i] = c.busID
		}
		increments = LCMPrime(matchedBusIDs)
	}

	x := 1
	for (lastTSMatch+x*increments+constraints[currBusSlot].departureDiff)%constraints[currBusSlot].busID != 0 {
		x++
	}
	newTSMatch := lastTSMatch + x*increments

	if currBusSlot < len(constraints)-1 {
		return findTimestampMatchingDepartureTimes(currBusSlot+1, newTSMatch, constraints)
	}
	return newTSMatch
}

func parseScheduleConstraintsFromFile() []scheduleConstraint {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseScheduleConstraints(input)
}

func parseScheduleConstraints(input []byte) []scheduleConstraint {

	lines := strings.Split(string(input), "\n")

	timeSlice := strings.Split(lines[1], ",")
	constraints := []scheduleConstraint{}

	for i, t := range timeSlice {
		if t == "x" {
			continue
		}
		busID, _ := strconv.Atoi(t)
		constraints = append(constraints, scheduleConstraint{busID: busID, departureDiff: i})
	}

	return constraints
}

func (s *schedule) getEarliestBusIDAndDepartureTime() (int, int) {
	earliestDep := 2 * s.earliestDeparture
	earliestBusID := 0

	for _, busID := range s.times {
		ed := int(math.Ceil(float64(s.earliestDeparture)/float64(busID))) * busID
		if ed < earliestDep {
			earliestDep = ed
			earliestBusID = busID
		}
	}
	return earliestBusID, earliestDep
}

func parseScheduleFromFile() *schedule {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseSchedule(input)
}

func parseSchedule(input []byte) *schedule {

	lines := strings.Split(string(input), "\n")
	earliestDeparture, _ := strconv.Atoi(lines[0])

	timeline := strings.ReplaceAll(lines[1], ",x", "")
	timeSlice := strings.Split(timeline, ",")
	times := make([]int, len(timeSlice))

	for i, t := range timeSlice {
		times[i], _ = strconv.Atoi(t)
	}

	return &schedule{earliestDeparture: earliestDeparture, times: times}
}

func LCMPrime(nums []int) int {
	result := 1
	for _, n := range nums {
		result *= n
	}

	return result
}
