package main

import (
	"testing"
)

func Test_schedule_getEarliestBusIDAndDepartureTime(t *testing.T) {
	tests := []struct {
		name     string
		schedule *schedule
		want     int
		want1    int
	}{
		{name: "aocExample", schedule: parseSchedule([]byte(`939
7,13,x,x,59,x,31,19`)), want: 59, want1: 944},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.schedule.getEarliestBusIDAndDepartureTime()
			if got != tt.want {
				t.Errorf("schedule.getEarliestBusIDAndDepartureTime() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("schedule.getEarliestBusIDAndDepartureTime() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestLCMPrime(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "1", args: args{nums: []int{7, 13, 19}}, want: 1729},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LCMPrime(tt.args.nums); got != tt.want {
				t.Errorf("LCMPrime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findTimestampMatchingDepartureTimes(t *testing.T) {
	type args struct {
		currBusSlot int
		lastTSMatch int
		constraints []scheduleConstraint
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "aocExample", args: args{currBusSlot: 0, lastTSMatch: 0, constraints: parseScheduleConstraints([]byte(`939
7,13,x,x,59,x,31,19`))}, want: 1068781},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findTimestampMatchingDepartureTimes(tt.args.currBusSlot, tt.args.lastTSMatch, tt.args.constraints); got != tt.want {
				t.Errorf("findTimestampMatchingDepartureTimes() = %v, want %v", got, tt.want)
			}
		})
	}
}
