package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type treeMap [][]rune

const (
	treeChar rune = '#'
	openChar rune = '.'
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 03: Toboggan Trajectory")

	// parse the tree input map
	tm := parseTreeMap()

	// part 1
	fmt.Printf("Part1: %d (encountered trees with slope [3 1])\n", tm.slideSlopeAndSumTrees(0, 0, 3, 1))

	// part 2
	slopes := [][]int{
		[]int{1, 1},
		[]int{3, 1},
		[]int{5, 1},
		[]int{7, 1},
		[]int{1, 2},
	}

	totalTrees := 1
	for _, s := range slopes {
		totalTrees *= tm.slideSlopeAndSumTrees(0, 0, s[0], s[1])
	}

	fmt.Printf("Part2: %d (encountered trees with slopes: %v)\n", totalTrees, slopes)
}

func parseTreeMap() treeMap {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")

	treeMap := make([][]rune, len(lines)-1)
	for i, l := range lines {
		if l != "" {
			treeMap[i] = []rune(l)
		}
	}
	return treeMap
}

func (t treeMap) slideSlopeAndSumTrees(currentX, currentY, slopeX, slopeY int) int {
	if currentY > len(t)-1 {
		return 0
	}

	if t.isTree(currentX, currentY) {
		return 1 + t.slideSlopeAndSumTrees(currentX+slopeX, currentY+slopeY, slopeX, slopeY)
	}
	return t.slideSlopeAndSumTrees(currentX+slopeX, currentY+slopeY, slopeX, slopeY)
}

func (t treeMap) String() string {
	var sb strings.Builder
	for _, s := range t {
		sb.WriteString(fmt.Sprintf("%s\n", string(s)))
	}
	return sb.String()
}

func (t treeMap) isTree(x, y int) bool {
	mapWidth := len(t[0])
	if x < mapWidth {
		return t[y][x] == treeChar
	}
	return t[y][x%mapWidth] == treeChar
}
