package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_treeMap_isTree(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		t    treeMap
		args args
		want bool
	}{
		{name: "1", t: [][]rune{[]rune{openChar, treeChar, openChar, openChar}}, args: args{x: 0, y: 0}, want: false},
		{name: "2", t: [][]rune{[]rune{openChar, treeChar, openChar, openChar}}, args: args{x: 1, y: 0}, want: true},
		{name: "3", t: [][]rune{[]rune{openChar, treeChar, openChar, openChar}}, args: args{x: 4, y: 0}, want: false},
		{name: "4", t: [][]rune{[]rune{openChar, treeChar, openChar, openChar}}, args: args{x: 5, y: 0}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.isTree(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("treeMap.isTree() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseTreeMap(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.True(t, len(parseTreeMap()) > 10)
		})
	}
}

func Test_treeMap_slideSlopeAndSumTrees(t *testing.T) {
	type args struct {
		currentX int
		currentY int
		slopeX   int
		slopeY   int
	}
	tests := []struct {
		name string
		t    treeMap
		args args
		want int
	}{
		{name: "1", t: [][]rune{
			[]rune{openChar, treeChar, openChar, openChar},
			[]rune{openChar, treeChar, openChar, openChar},
			[]rune{openChar, treeChar, openChar, openChar},
		},
			args: args{currentX: 0, currentY: 0, slopeX: 1, slopeY: 1}, want: 1},
		{name: "2", t: [][]rune{
			[]rune{openChar, treeChar, openChar, openChar},
			[]rune{openChar, treeChar, openChar, openChar},
			[]rune{openChar, treeChar, treeChar, openChar},
		},
			args: args{currentX: 0, currentY: 0, slopeX: 1, slopeY: 1}, want: 2},
		{name: "2", t: [][]rune{
			[]rune{openChar, treeChar},
			[]rune{openChar, treeChar},
			[]rune{openChar, treeChar},
			[]rune{openChar, treeChar},
		},
			args: args{currentX: 0, currentY: 0, slopeX: 1, slopeY: 1}, want: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.slideSlopeAndSumTrees(tt.args.currentX, tt.args.currentY, tt.args.slopeX, tt.args.slopeY); got != tt.want {
				t.Errorf("treeMap.slideSlopeAndSumTrees() = %v, want %v", got, tt.want)
			}
		})
	}
}
