# Advent of Code 2020 - Day 3: Toboggan Trajectory

- Puzzle: <https://adventofcode.com/2020/day/3>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

