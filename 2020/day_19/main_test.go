package main

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parseInput(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want map[int]rule
	}{
		{name: "1", args: args{input: []byte(`
0: 4 1 5`)}, want: map[int]rule{0: rule{id: 0, ruleRef: [][]int{[]int{4, 1, 5}}}}},
		{name: "2", args: args{input: []byte(`
1: 2 3 | 3 2`)}, want: map[int]rule{1: rule{id: 1, ruleRef: [][]int{[]int{2, 3}, []int{3, 2}}}}},
		{name: "3", args: args{input: []byte(`
1: 2 | 3`)}, want: map[int]rule{1: rule{id: 1, ruleRef: [][]int{[]int{2}, []int{3}}}}},
		{name: "3", args: args{input: []byte(`
8: 42`)}, want: map[int]rule{8: rule{id: 8, ruleRef: [][]int{[]int{42}}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseInput(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseInput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_match(t *testing.T) {
	type args struct {
		rules  []byte
		input  string
		ruleID int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "1", args: args{rules: []byte(`
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"`), input: "ababbb", ruleID: 0}, want: []int{6}},
		{name: "2", args: args{rules: []byte(`
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"`), input: "bababa", ruleID: 0}, want: []int{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := parseInput(tt.args.rules)
			got := match(r, tt.args.input, tt.args.ruleID)
			assert.ElementsMatch(t, got, tt.want)
		})
	}
}

func Test_countMatches(t *testing.T) {
	type args struct {
		rules  []byte
		output []string
		ruleID int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "aocExample", args: args{rules: []byte(`
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"`), output: []string{"ababbb", "bababa", "abbbab", "aaabbb", "aaaabbb"}, ruleID: 0}, want: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := parseInput(tt.args.rules)
			if got := countMatches(r, tt.args.output, tt.args.ruleID); got != tt.want {
				t.Errorf("countMatches() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countMatchesPart2(t *testing.T) {
	type args struct {
		rules  []byte
		output []byte
		ruleID int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "aocExample", args: args{rules: []byte(`
42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1`),
			output: []byte(`
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba`), ruleID: 0}, want: 12},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := parseInputAndAdjust(tt.args.rules)
			fmt.Printf("r = %+v\n", r)
			o := parseOutput(tt.args.output)
			fmt.Printf("o = %+v\n", o)
			if got := countMatches(r, o, tt.args.ruleID); got != tt.want {
				t.Errorf("countMatches() = %v, want %v", got, tt.want)
			}
		})
	}
}
