package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type rule struct {
	id      int
	ruleRef [][]int
	char    rune
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 19: Monster Messages")

	rules := parseInputFromFile()
	out := parseOutputFromFile()
	sumMatches := countMatches(rules, out, 0)

	fmt.Printf("Part1: %d (messages completely match by rule 0)\n", sumMatches)

	nRules := parseInputAndAdjustFromFile()
	sumNewMatches := countMatches(nRules, out, 0)

	fmt.Printf("Part2: %d (messages completely match by rule 0 with loops)\n", sumNewMatches)
}

func countMatches(rules map[int]rule, output []string, ruleID int) int {
	sum := 0
	for _, o := range output {
		res := match(rules, o, ruleID)
		for _, r := range res {
			if r == len(o) {
				sum++
				break
			}
		}
	}
	return sum
}

// Thanks Georg for the help: https://www.youtube.com/watch?v=OxDp11u-GUo
func match(rules map[int]rule, input string, ruleID int) []int {
	rule := rules[ruleID]

	if rule.char != 0 {
		if len(input) > 0 && []rune(input)[0] == rule.char {
			return []int{1}
		}
		return []int{}
	}

	nRet := []int{}
	for _, opt := range rule.ruleRef {
		sum := []int{0}
		for _, r := range opt {
			nSum := []int{}
			for _, s := range sum {
				res := match(rules, input[s:], r)
				for _, r := range res {
					nSum = append(nSum, r+s)
				}
			}
			sum = nSum
		}
		nRet = append(nRet, sum...)
	}
	return nRet
}

func parseOutput(input []byte) []string {
	lines := strings.Split(string(input), "\n")

	start := 0
	for i, l := range lines {
		if l == "" {
			start = i
			break
		}
	}

	out := []string{}
	for i := start + 1; i < len(lines); i++ {
		if lines[i] != "" {
			out = append(out, lines[i])
		}
	}
	return out
}

func parseOutputFromFile() []string {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseOutput(input)
}

func parseInputFromFile() map[int]rule {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInput(input)
}

func parseInputAndAdjustFromFile() map[int]rule {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInputAndAdjust(input)
}

func parseInputAndAdjust(input []byte) map[int]rule {
	rules := parseInput(input)
	rules[8] = rule{id: 8, ruleRef: [][]int{[]int{42}, []int{42, 8}}}
	rules[11] = rule{id: 11, ruleRef: [][]int{[]int{42, 31}, []int{42, 11, 31}}}
	return rules
}

func parseInput(input []byte) map[int]rule {
	lines := strings.Split(string(input), "\n")
	rules := map[int]rule{}

	for _, l := range lines {
		reRuleID := regexp.MustCompile(`^(\d+):.*`)
		ruleIDMatch := reRuleID.FindStringSubmatch(l)
		if len(ruleIDMatch) != 2 {
			continue
		}
		ruleID, _ := strconv.Atoi(ruleIDMatch[1])

		reRuleRef := regexp.MustCompile(`^\d+:(\s\d+)+$`)
		ruleRefs := reRuleRef.FindStringSubmatch(l)
		if len(ruleRefs) > 1 {
			refIDs := []int{}
			for _, ref := range strings.Split(strings.ReplaceAll(ruleRefs[0], fmt.Sprintf("%d: ", ruleID), ""), " ") {
				id, _ := strconv.Atoi(ref)
				refIDs = append(refIDs, id)
			}
			rules[ruleID] = rule{id: ruleID, ruleRef: [][]int{refIDs}}
			continue
		}

		// TODO optimize
		reOptRuleRef := regexp.MustCompile(`^\d+:\s(\d+)\s(\d+)\s\|\s(\d+)\s(\d+)`)
		optRuleRefs := reOptRuleRef.FindStringSubmatch(l)
		if len(optRuleRefs) == 5 {
			a, _ := strconv.Atoi(optRuleRefs[1])
			b, _ := strconv.Atoi(optRuleRefs[2])
			c, _ := strconv.Atoi(optRuleRefs[3])
			d, _ := strconv.Atoi(optRuleRefs[4])
			rules[ruleID] = rule{id: ruleID, ruleRef: [][]int{[]int{a, b}, []int{c, d}}}
			continue
		}

		reOptRuleRef2 := regexp.MustCompile(`^\d+:\s(\d+)\s\|\s(\d+)`)
		optRuleRefs2 := reOptRuleRef2.FindStringSubmatch(l)
		if len(optRuleRefs2) == 3 {
			a, _ := strconv.Atoi(optRuleRefs2[1])
			b, _ := strconv.Atoi(optRuleRefs2[2])
			rules[ruleID] = rule{id: ruleID, ruleRef: [][]int{[]int{a}, []int{b}}}
			continue
		}

		reChar := regexp.MustCompile(`^\d+:\s"(\w)"`)
		charRefs := reChar.FindStringSubmatch(l)
		if len(charRefs) == 2 {
			rules[ruleID] = rule{id: ruleID, char: []rune(charRefs[1])[0]}
			continue
		}
	}
	return rules
}

func calcLine(line string, advancedMath bool) int {
	plainExp := replaceParanthesisWithSolution(line, advancedMath)
	res := calculateExpression(plainExp, advancedMath)

	resInt, _ := strconv.Atoi(res)
	return resInt
}

func replaceParanthesisWithSolution(line string, advancedMath bool) string {
	r := regexp.MustCompile(`[\(\)]`)
	p := r.Find([]byte(line))
	if p == nil {
		return line
	}

	r = regexp.MustCompile(`\(([*+\s0-9]+)\)`)
	expInParanthesis := r.FindStringSubmatch(line)
	res := calculateExpression(expInParanthesis[1], advancedMath)

	return replaceParanthesisWithSolution(strings.Replace(line, expInParanthesis[0], res, 1), advancedMath)
}

func calculateExpression(exp string, advancedMath bool) string {
	r := regexp.MustCompile(`[\*\+]`)
	op := r.Find([]byte(exp))
	if op == nil {
		return exp
	}

	res := 0
	nextExp := []string{}
	if advancedMath {
		reNextExp := regexp.MustCompile(`(\d+)(\+)(\d+)`)
		nextExp = reNextExp.FindStringSubmatch(exp)
		if len(nextExp) != 4 {
			reNextExp := regexp.MustCompile(`(\d+)(\*)(\d+)`)
			nextExp = reNextExp.FindStringSubmatch(exp)
		}
		res = calc(nextExp[1], nextExp[2], nextExp[3])

	} else {
		reNextExp := regexp.MustCompile(`(\d+)([\+\*])(\d+)`)
		nextExp = reNextExp.FindStringSubmatch(exp)
		res = calc(nextExp[1], nextExp[2], nextExp[3])
	}

	return calculateExpression(strings.Replace(exp, nextExp[0], strconv.Itoa(res), 1), advancedMath)
}

func calc(a, op, b string) int {
	numA, _ := strconv.Atoi(a)
	numB, _ := strconv.Atoi(b)

	switch op {
	case "+":
		return numA + numB
	case "*":
		return numA * numB
	}
	return 0
}
