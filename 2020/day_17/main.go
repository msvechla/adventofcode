package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type conwaycubes struct {
	world map[int]map[int]map[int]bool
	minX  int
	maxX  int
	minY  int
	maxY  int
}

type conwaycubes4d struct {
	world map[int]map[int]map[int]map[int]bool
}

const (
	TypeInactiveCube = '.'
	TypeActiveCube   = '#'
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 17: Conway Cubes")
	cc := parseConwayCubesFromFile()
	numActive := cc.numActiveAfterCycles(6)

	fmt.Printf("Part1: %d (cubes left in the active state after the sixth cycle)\n", numActive)

	c3d := parseConwayCubesFromFile()
	c4d := conwaycubes4d{}
	c4d.world = make(map[int]map[int]map[int]map[int]bool)
	c4d.world[0] = c3d.world
	num4dAcrive := c4d.numActiveAfterCycles(6)
	fmt.Printf("Part2: %d (cubes left in the active state after the sixth cycle in 4D)\n", num4dAcrive)
}

func (cc *conwaycubes) numActiveAfterCycles(cycles int) int {
	for i := 0; i < cycles; i++ {
		cc.cycle()
	}

	totalActive := 0
	for _, z := range cc.world {
		for _, y := range z {
			for _, c := range y {
				if c {
					totalActive++
				}
			}
		}
	}
	return totalActive
}

func parseConwayCubesFromFile() conwaycubes {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseConwayCubes(input)
}

func (cc *conwaycubes) setActive(x, y, z int) {
	if _, exists := cc.world[z]; !exists {
		cc.world[z] = make(map[int]map[int]bool)
	}
	if _, exists := cc.world[z][y]; !exists {
		cc.world[z][y] = make(map[int]bool)
	}

	cc.world[z][y][x] = true
	if x < cc.minX {
		cc.minX = x
	}
	if x > cc.maxX {
		cc.maxX = x
	}
	if y < cc.minY {
		cc.minY = y
	}
	if y > cc.maxY {
		cc.maxY = y
	}
}

func parseConwayCubes(input []byte) conwaycubes {

	lines := strings.Split(string(input), "\n")
	cc := conwaycubes{}
	cc.world = make(map[int]map[int]map[int]bool)

	countRows := 0
	for _, l := range lines {
		if l == "" {
			continue
		}
		for x, c := range l {
			if c == TypeActiveCube {
				cc.setActive(x, countRows, 0)
			}
		}
		countRows++
	}
	return cc
}

func (cc *conwaycubes) width() int {
	return len(cc.world[0][0])
}
func (cc *conwaycubes) height() int {
	return len(cc.world[0])
}

func (cc *conwaycubes) cycle() {
	adjacentCubes := conwaycubes{}
	adjacentCubes.world = make(map[int]map[int]map[int]bool)

	newCubes := conwaycubes{}
	newCubes.world = make(map[int]map[int]map[int]bool)

	for cZ, z := range cc.world {
		for cY, y := range z {
			for cX, c := range y {
				if c {
					// get neighbours of all current active cubes
					ad := cc.getAdjacentCubes(cX, cY, cZ)
					for acZ, az := range ad {
						for acY, ay := range az {
							for acX, ac := range ay {
								// neihbour was not yet checked
								if _, exists := adjacentCubes.world[acZ][acY][acX]; !exists {
									if ac {
										// mark neighbour as checked
										adjacentCubes.setActive(acX, acY, acZ)
										numActive := cc.getNumAdjacentActiveCubes(acX, acY, acZ)
										if cc.world[acZ][acY][acX] == true {
											if numActive >= 2 && numActive <= 3 {
												newCubes.setActive(acX, acY, acZ)
											}
										} else {
											if numActive == 3 {
												newCubes.setActive(acX, acY, acZ)
											}
										}
									}
								}
							}
						}
					}

					numActive := cc.getNumAdjacentActiveCubes(cX, cY, cZ)
					if numActive >= 2 && numActive <= 3 {
						newCubes.setActive(cX, cY, cZ)
					}
				}
			}
		}
	}
	*cc = newCubes
}

func (cc conwaycubes) getAdjacentCubes(x, y, z int) map[int]map[int]map[int]bool {
	adjacentCubes := conwaycubes{}
	adjacentCubes.world = make(map[int]map[int]map[int]bool)
	posModifier := []int{0, -1, 1}
	for _, cZ := range posModifier {
		for _, cX := range posModifier {
			for _, cY := range posModifier {
				if cZ == 0 && cX == 0 && cY == 0 {
					continue
				}
				adjacentCubes.setActive(x+cX, y+cY, z+cZ)
			}
		}
	}
	return adjacentCubes.world
}

func (cc conwaycubes) getNumAdjacentActiveCubes(x, y, z int) int {
	adjacentCubes := 0
	posModifier := []int{0, -1, 1}
	for _, cZ := range posModifier {
		for _, cX := range posModifier {
			for _, cY := range posModifier {
				if cZ == 0 && cX == 0 && cY == 0 {
					continue
				}
				if cc.world[z+cZ][y+cY][x+cX] == true {
					adjacentCubes++
				}
			}
		}
	}
	return adjacentCubes
}

func (cc conwaycubes) String() string {
	var sb strings.Builder
	for i, _ := range cc.world {
		sb.WriteString(fmt.Sprintf("\nz: %d\n", i))
		for y := cc.minY; y <= cc.maxY; y++ {
			sb.WriteString("\n")
			for x := cc.minX; x <= cc.maxX; x++ {
				if cc.world[i][y][x] == true {
					sb.WriteString(string(TypeActiveCube))
				} else {
					sb.WriteString(string(TypeInactiveCube))
				}
			}
		}
	}
	return sb.String()
}

// ---------- PART 2 ----------
func (cc *conwaycubes4d) numActiveAfterCycles(cycles int) int {
	for i := 0; i < cycles; i++ {
		cc.cycle()
	}

	totalActive := 0
	for _, w := range cc.world {
		for _, z := range w {
			for _, y := range z {
				for _, x := range y {
					if x {
						totalActive++
					}
				}
			}
		}
	}
	return totalActive
}

func (cc *conwaycubes4d) setActive(x, y, z, w int) {
	if _, exists := cc.world[w]; !exists {
		cc.world[w] = make(map[int]map[int]map[int]bool)
	}
	if _, exists := cc.world[w][z]; !exists {
		cc.world[w][z] = make(map[int]map[int]bool)
	}
	if _, exists := cc.world[w][z][y]; !exists {
		cc.world[w][z][y] = make(map[int]bool)
	}

	cc.world[w][z][y][x] = true
}

func (cc *conwaycubes4d) width() int {
	return len(cc.world[0][0][0])
}
func (cc *conwaycubes4d) height() int {
	return len(cc.world[0][0])
}

func (cc *conwaycubes4d) cycle() {
	adjacentCubes := conwaycubes4d{}
	adjacentCubes.world = make(map[int]map[int]map[int]map[int]bool)

	newCubes := conwaycubes4d{}
	newCubes.world = make(map[int]map[int]map[int]map[int]bool)

	for cW, w := range cc.world {
		for cZ, z := range w {
			for cY, y := range z {
				for cX, c := range y {
					if c {
						// get neighbours of all current active cubes
						ad := cc.getAdjacentCubes(cX, cY, cZ, cW)
						for acW, aw := range ad {
							for acZ, az := range aw {
								for acY, ay := range az {
									for acX, ac := range ay {
										// neihbour was not yet checked
										if _, exists := adjacentCubes.world[acW][acZ][acY][acX]; !exists {
											if ac {
												// mark neighbour as checked
												adjacentCubes.setActive(acX, acY, acZ, acW)
												numActive := cc.getNumAdjacentActiveCubes(acX, acY, acZ, acW)
												if cc.world[acW][acZ][acY][acX] == true {
													if numActive >= 2 && numActive <= 3 {
														newCubes.setActive(acX, acY, acZ, acW)
													}
												} else {
													if numActive == 3 {
														newCubes.setActive(acX, acY, acZ, acW)
													}
												}
											}
										}
									}
								}
							}
						}

						numActive := cc.getNumAdjacentActiveCubes(cX, cY, cZ, cW)
						if numActive >= 2 && numActive <= 3 {
							newCubes.setActive(cX, cY, cZ, cW)
						}
					}
				}
			}
		}
	}
	*cc = newCubes
}

func (cc conwaycubes4d) getAdjacentCubes(x, y, z, w int) map[int]map[int]map[int]map[int]bool {
	adjacentCubes := conwaycubes4d{}
	adjacentCubes.world = make(map[int]map[int]map[int]map[int]bool)
	posModifier := []int{0, -1, 1}
	for _, cW := range posModifier {
		for _, cZ := range posModifier {
			for _, cX := range posModifier {
				for _, cY := range posModifier {
					if cZ == 0 && cX == 0 && cY == 0 && cW == 0 {
						continue
					}
					adjacentCubes.setActive(x+cX, y+cY, z+cZ, w+cW)
				}
			}
		}
	}
	return adjacentCubes.world
}

func (cc conwaycubes4d) getNumAdjacentActiveCubes(x, y, z, w int) int {
	adjacentCubes := 0
	posModifier := []int{0, -1, 1}
	for _, cW := range posModifier {
		for _, cZ := range posModifier {
			for _, cX := range posModifier {
				for _, cY := range posModifier {
					if cZ == 0 && cX == 0 && cY == 0 && cW == 0 {
						continue
					}
					if cc.world[w+cW][z+cZ][y+cY][x+cX] == true {
						adjacentCubes++
					}
				}
			}
		}
	}
	return adjacentCubes
}
