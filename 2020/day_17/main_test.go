package main

import (
	"testing"
)

func Test_conwaycubes_getNumAdjacentActiveCubes(t *testing.T) {
	type args struct {
		x int
		y int
		z int
	}
	tests := []struct {
		name string
		cc   conwaycubes
		args args
		want int
	}{
		{name: "aocExample", args: args{x: 2, y: 1, z: 0}, cc: parseConwayCubes([]byte(`
.#.
..#
###`)), want: 3},
		{name: "aocExampleZ-1", args: args{x: 2, y: 1, z: -1}, cc: parseConwayCubes([]byte(`
.#.
..#
###`)), want: 4},
		{name: "aocExampleZ+1", args: args{x: 2, y: 1, z: 1}, cc: parseConwayCubes([]byte(`
.#.
..#
###`)), want: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cc.getNumAdjacentActiveCubes(tt.args.x, tt.args.y, tt.args.z); got != tt.want {
				t.Errorf("conwaycubes.getNumAdjacentActiveCubes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_conwaycubes_numActiveAfterCycles(t *testing.T) {
	type args struct {
		cycles int
	}
	tests := []struct {
		name string
		cc   conwaycubes
		args args
		want int
	}{
		{name: "aocExample", args: args{cycles: 6}, cc: parseConwayCubes([]byte(`
.#.
..#
###`)), want: 112},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cc.numActiveAfterCycles(tt.args.cycles); got != tt.want {
				t.Errorf("conwaycubes.numActiveAfterCycles() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_conwaycubes4d_numActiveAfterCycles(t *testing.T) {
	c4d := parseConwayCubes([]byte(`
.#.
..#
###`))
	type args struct {
		cycles int
	}
	tests := []struct {
		name string
		cc   conwaycubes4d
		args args
		want int
	}{
		{name: "aocExample", args: args{cycles: 6}, cc: conwaycubes4d{world: map[int]map[int]map[int]map[int]bool{0: c4d.world}}, want: 848},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cc.numActiveAfterCycles(tt.args.cycles); got != tt.want {
				t.Errorf("conwaycubes.numActiveAfterCycles() = %v, want %v", got, tt.want)
			}
		})
	}
}
