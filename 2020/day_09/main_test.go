package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_findTwoNumberSum(t *testing.T) {
	type args struct {
		numbers []int
		target  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "1", args: args{numbers: []int{1, 2, 3, 4, 5}, target: 5}, want: []int{2, 3}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := findTwoNumberSum(tt.args.numbers, tt.args.target)
			assert.ElementsMatch(t, got, tt.want)
		})
	}
}

func Test_parseNumbersFromFile(t *testing.T) {
	tests := []struct {
		name string
		want []int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseNumbersFromFile(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseNumbersFromFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseNumbers(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "test", args: args{input: []byte(`
1
12
42`)}, want: []int{1, 12, 42}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseNumbers(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findNonSumTargetWithPreamble(t *testing.T) {
	type args struct {
		numbers  []int
		preamble int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "test", args: args{numbers: parseNumbers([]byte(`
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`)), preamble: 5}, want: 127},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findNonSumTargetWithPreamble(tt.args.numbers, tt.args.preamble); got != tt.want {
				t.Errorf("findNonSumTargetWithPreamble() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findContiguousSum(t *testing.T) {
	type args struct {
		numbers []int
		target  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "test", args: args{numbers: parseNumbers([]byte(`
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`)), target: 127}, want: []int{15, 25, 47, 40}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _, _ := findContiguousSum(tt.args.numbers, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findContiguousSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
