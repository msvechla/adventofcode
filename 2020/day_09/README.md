# Advent of Code 2020 - Day 9: Encoding Error

- Puzzle: <https://adventofcode.com/2020/day/9>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

