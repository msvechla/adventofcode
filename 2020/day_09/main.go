package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const dataPortPreamble = 25

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 09: Encoding Error")
	numbers := parseNumbersFromFile()
	nonSum := findNonSumTargetWithPreamble(numbers, dataPortPreamble)
	fmt.Printf("Part1: %d (first number that does not have a target sum with preamble)\n", nonSum)

	cSum, low, high := findContiguousSum(numbers, nonSum)
	fmt.Printf("Part2: %d (sum of lowest and highest number of contiguous numbers: %v)\n", low+high, cSum)
}

func parseNumbersFromFile() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseNumbers(input)
}

func findContiguousSum(numbers []int, target int) ([]int, int, int) {
	for startIndex := range numbers {
		cSum := []int{}
		currentSum := 0
		low := 9999999999
		high := 0

		for i := startIndex; i < len(numbers); i++ {
			currentSum += numbers[i]
			if currentSum > target {
				break
			}
			cSum = append(cSum, numbers[i])
			if numbers[i] < low {
				low = numbers[i]
			}
			if numbers[i] > high {
				high = numbers[i]
			}

			if currentSum == target {
				return cSum, low, high
			}
		}
	}
	return []int{}, 0, 0
}

func findNonSumTargetWithPreamble(numbers []int, preamble int) int {
	for i := preamble; i < len(numbers); i++ {
		sum := findTwoNumberSum(numbers[i-(preamble):i], numbers[i])
		if len(sum) != 2 {
			return numbers[i]
		}
	}
	return 0
}

func parseNumbers(input []byte) []int {

	lines := strings.Split(string(input), "\n")
	numbers := make([]int, len(lines)-1)

	iCount := 0
	for _, l := range lines {
		if l == "" {
			continue
		}

		numbers[iCount], _ = strconv.Atoi(l)
		iCount++
	}
	return numbers
}

func findTwoNumberSum(numbers []int, target int) []int {
	hashMap := make(map[int]bool, len(numbers))

	for _, e := range numbers {
		potentialMatch := target - e
		if _, exists := hashMap[potentialMatch]; exists {
			return []int{e, potentialMatch}
		}
		hashMap[e] = true
	}
	return []int{}
}
