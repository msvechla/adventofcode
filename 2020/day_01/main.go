package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 01: Report Repair")
	expenses := getExpensesInput()
	match := find2020Sum(expenses)
	fmt.Printf("Part1: %d (product of two expenses that sum to 2020)\n", match[0]*match[1])

	threeMatches := findThreeNumber2020Sum(expenses)
	fmt.Printf("Part2: %d (product of three expenses that sum to 2020)\n", threeMatches[0]*threeMatches[1]*threeMatches[2])
}

func find2020Sum(input []int) []int {
	hashMap := make(map[int]bool, len(input))

	for _, e := range input {
		potentialMatch := 2020 - e
		if _, exists := hashMap[potentialMatch]; exists {
			return []int{e, potentialMatch}
		}
		hashMap[e] = true
	}
	return []int{}
}

func findThreeNumber2020Sum(array []int) []int {
	sort.Ints(array)

	for currentP := 0; currentP < len(array)-2; currentP++ {
		leftP := currentP + 1
		rightP := len(array) - 1

		for leftP < rightP {
			leftNum := array[leftP]
			rightNum := array[rightP]
			currentNum := array[currentP]
			currentSum := currentNum + leftNum + rightNum

			if currentSum == 2020 {
				return []int{currentNum, leftNum, rightNum}
			}

			if currentSum < 2020 {
				leftP++
			} else if currentSum > 2020 {
				rightP--
			}
		}
	}

	return []int{}
}

func getExpensesInput() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	eLines := strings.Split(string(input), "\n")
	expenses := make([]int, len(eLines)-1)

	for i, e := range eLines {
		if e == "" {
			continue
		}

		expense, err := strconv.Atoi(e)
		if err != nil {
			log.Fatal(err)
		}

		expenses[i] = expense
	}
	return expenses
}
