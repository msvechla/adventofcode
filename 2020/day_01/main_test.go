package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_find2020Sum(t *testing.T) {
	type args struct {
		input []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "1", args: args{input: []int{1721, 979, 366, 299, 675, 1456}}, want: []int{1721, 299}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.ElementsMatch(t, find2020Sum(tt.args.input), tt.want)
		})
	}
}

func Test_findThreeNumber2020Sum(t *testing.T) {
	type args struct {
		array []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "1", args: args{array: []int{1721, 979, 366, 299, 675, 1456}}, want: []int{979, 366, 675}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.ElementsMatch(t, findThreeNumber2020Sum(tt.args.array), tt.want)
		})
	}
}
