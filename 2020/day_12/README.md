# Advent of Code 2020 - Day 12: Rain Risk

- Puzzle: <https://adventofcode.com/2020/day/12>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

