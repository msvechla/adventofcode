package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

type instruction struct {
	direction rune
	value     int
}

type ship struct {
	position         map[int]int
	direction        int
	waypointPosition map[int]int
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 12: Rain Risk")
	instructions := parseInstructionsFromFile()
	s := newShip()
	s.navigate(instructions)
	fmt.Printf("Part1: %.0f (Manhattan distance between location and the ship's starting position)\n", s.manhattenDistanceFromStart())
	s = newShip()
	s.navigateWaypoint(instructions)
	fmt.Printf("Part2: %.0f (Manhattan distance between location and the ship's starting position (wayoint navigation))\n", s.manhattenDistanceFromStart())
}

func newShip() *ship {
	return &ship{position: map[int]int{0: 0, 1: 0, 2: 0, 3: 0}, waypointPosition: map[int]int{0: 1, 1: 10, 2: 0, 3: 0}, direction: directionRuneToInt('E')}
}

func (s *ship) manhattenDistanceFromStart() float64 {
	return math.Abs(float64(s.position[directionRuneToInt('N')])-
		float64(s.position[directionRuneToInt('S')])) +
		math.Abs(float64(s.position[directionRuneToInt('E')])-
			float64(s.position[directionRuneToInt('W')]))
}

func (s *ship) navigateWaypoint(instructions []instruction) map[int]int {
	for _, i := range instructions {
		switch i.direction {
		case 'L', 'R':
			s.turnWaypoint(i.direction, i.value)
		case 'F':
			for d, _ := range s.position {
				s.position[d] += s.waypointPosition[d] * i.value
			}
		case 'N', 'E', 'S', 'W':
			s.waypointPosition[directionRuneToInt(i.direction)] += i.value
		}
	}
	return s.position
}

func (s *ship) navigate(instructions []instruction) map[int]int {
	for _, i := range instructions {
		switch i.direction {
		case 'L', 'R':
			s.turn(i.direction, i.value)
		case 'F':
			s.position[s.direction] += i.value
		case 'N', 'E', 'S', 'W':
			s.position[directionRuneToInt(i.direction)] += i.value
		}
	}
	return s.position
}

func directionRuneToInt(d rune) int {
	switch d {
	case 'N':
		return 0
	case 'E':
		return 1
	case 'S':
		return 2
	case 'W':
		return 3
	}
	return 0
}

func (s *ship) turnWaypoint(direction rune, value int) map[int]int {
	modifier := 0
	switch direction {
	case 'L':
		modifier = -1
	case 'R':
		modifier = 1
	}

	newWP := make(map[int]int, 4)
	for d, v := range s.waypointPosition {
		newDirection := d + (value/90)*modifier
		if newDirection < 0 {
			newDirection = 4 + newDirection
		}
		if newDirection > 3 {
			newDirection = newDirection - 4
		}
		newWP[newDirection] = v
	}
	s.waypointPosition = newWP
	return newWP
}

func (s *ship) turn(direction rune, value int) int {
	newDirection := 0
	switch direction {
	case 'L':
		newDirection = s.direction - (value / 90)
		if newDirection < 0 {
			newDirection = 4 + newDirection
		}
	case 'R':
		newDirection = s.direction + (value / 90)
		if newDirection > 3 {
			newDirection = newDirection - 4
		}
	}
	s.direction = newDirection
	return newDirection
}

func parseInstructionsFromFile() []instruction {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInstructions(input)
}

func parseInstructions(input []byte) []instruction {

	lines := strings.Split(string(input), "\n")
	instructions := make([]instruction, len(lines)-1)

	countRows := 0
	for _, l := range lines {
		if l == "" {
			continue
		}
		direction := []rune(l)[:1]
		value, _ := strconv.Atoi(l[1:])
		instructions[countRows] = instruction{direction: direction[0], value: value}
		countRows++
	}
	return instructions
}

func (i instruction) String() string {
	return fmt.Sprintf("%s: %d", string(i.direction), i.value)
}
