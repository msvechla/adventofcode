package main

import (
	"reflect"
	"testing"
)

func Test_parseInstructionsFromFile(t *testing.T) {
	tests := []struct {
		name string
		want []instruction
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseInstructionsFromFile(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseInstructionsFromFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseInstructions(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []instruction
	}{
		{name: "aocExample", args: args{input: []byte(`
F10
N3
F7
R90
F11`)}, want: []instruction{
			{direction: 'F', value: 10},
			{direction: 'N', value: 3},
			{direction: 'F', value: 7},
			{direction: 'R', value: 90},
			{direction: 'F', value: 11},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseInstructions(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseInstructions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ship_turn(t *testing.T) {
	type fields struct {
		position  map[int]int
		direction int
	}
	type args struct {
		direction rune
		value     int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{name: "1", args: args{direction: 'L', value: 270}, fields: fields{direction: 1}, want: 2},
		{name: "2", args: args{direction: 'R', value: 270}, fields: fields{direction: 3}, want: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ship{
				position:  tt.fields.position,
				direction: tt.fields.direction,
			}
			if got := s.turn(tt.args.direction, tt.args.value); got != tt.want {
				t.Errorf("ship.turn() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ship_navigate(t *testing.T) {
	type fields struct {
		position  map[int]int
		direction int
	}
	type args struct {
		instructions []instruction
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[int]int
	}{
		{name: "aocExample", args: args{instructions: parseInstructions([]byte(`
F10
N3
F7
R90
F11`))}, fields: fields{position: map[int]int{}, direction: 1}, want: map[int]int{0: 3, 1: 17, 2: 11}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ship{
				position:  tt.fields.position,
				direction: tt.fields.direction,
			}
			if got := s.navigate(tt.args.instructions); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ship.navigate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ship_manhattenDistanceFromStart(t *testing.T) {
	type fields struct {
		position  map[int]int
		direction int
	}
	tests := []struct {
		name   string
		fields fields
		want   float64
	}{
		{name: "aocExample", fields: fields{position: map[int]int{0: 3, 1: 17, 2: 11}, direction: 1}, want: 25},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ship{
				position:  tt.fields.position,
				direction: tt.fields.direction,
			}
			if got := s.manhattenDistanceFromStart(); got != tt.want {
				t.Errorf("ship.manhattenDistanceFromStart() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ship_turnWaypoint(t *testing.T) {
	type fields struct {
		position         map[int]int
		direction        int
		waypointPosition map[int]int
	}
	type args struct {
		direction rune
		value     int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[int]int
	}{
		{name: "aocExample", fields: fields{waypointPosition: map[int]int{0: 4, 1: 10, 2: 0, 3: 0}}, args: args{direction: 'R', value: 90}, want: map[int]int{0: 0, 1: 4, 2: 10, 3: 0}},
		{name: "aocExample", fields: fields{waypointPosition: map[int]int{0: 4, 1: 10, 2: 0, 3: 0}}, args: args{direction: 'R', value: 180}, want: map[int]int{0: 0, 1: 0, 2: 4, 3: 10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ship{
				position:         tt.fields.position,
				direction:        tt.fields.direction,
				waypointPosition: tt.fields.waypointPosition,
			}
			if got := s.turnWaypoint(tt.args.direction, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ship.turnWaypoint() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ship_navigateWaypoint(t *testing.T) {
	type fields struct {
		position         map[int]int
		direction        int
		waypointPosition map[int]int
	}
	type args struct {
		instructions []instruction
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[int]int
	}{
		{name: "aocExample", args: args{instructions: parseInstructions([]byte(`
F10
N3
F7
R90
F11`))}, fields: fields{position: map[int]int{0: 0, 1: 0, 2: 0, 3: 0}, waypointPosition: map[int]int{0: 1, 1: 10, 2: 0, 3: 0}}, want: map[int]int{0: 38, 1: 214, 2: 110, 3: 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ship{
				position:         tt.fields.position,
				direction:        tt.fields.direction,
				waypointPosition: tt.fields.waypointPosition,
			}
			if got := s.navigateWaypoint(tt.args.instructions); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ship.navigateWaypoint() = %v, want %v", got, tt.want)
			}
		})
	}
}
