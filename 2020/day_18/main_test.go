package main

import (
	"testing"
)

func Test_calculateExpression(t *testing.T) {
	type args struct {
		exp          string
		advancedMath bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "1", args: args{exp: "1+2*3+4*5+6", advancedMath: false}, want: "71"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateExpression(tt.args.exp, tt.args.advancedMath); got != tt.want {
				t.Errorf("calculateExpression() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_replaceParanthesisWithSolution(t *testing.T) {
	type args struct {
		line         string
		advancedMath bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "1", args: args{line: "1+(2*3)+(4*(5+6))", advancedMath: false}, want: "1+6+44"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceParanthesisWithSolution(tt.args.line, tt.args.advancedMath); got != tt.want {
				t.Errorf("replaceParanthesisWithSolution() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calcLine(t *testing.T) {
	type args struct {
		line         string
		advancedMath bool
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "1", args: args{advancedMath: false, line: "1+(2*3)+(4*(5+6))"}, want: 51},
		{name: "2", args: args{advancedMath: false, line: "2*3+(4*5)"}, want: 26},
		{name: "3", args: args{advancedMath: false, line: "5+(8*3+9+3*4*3)"}, want: 437},
		{name: "4", args: args{advancedMath: false, line: "5*9*(7*3*3+9*3+(8+6*4))"}, want: 12240},
		{name: "5", args: args{advancedMath: false, line: "((2+4*9)*(6+9*8+6)+6)+2+4*2"}, want: 13632},
		{name: "1A", args: args{advancedMath: true, line: "1+(2*3)+(4*(5+6))"}, want: 51},
		{name: "2A", args: args{advancedMath: true, line: "2*3+(4*5)"}, want: 46},
		{name: "3A", args: args{advancedMath: true, line: "5+(8*3+9+3*4*3)"}, want: 1445},
		{name: "4A", args: args{advancedMath: true, line: "5*9*(7*3*3+9*3+(8+6*4))"}, want: 669060},
		{name: "5A", args: args{advancedMath: true, line: "((2+4*9)*(6+9*8+6)+6)+2+4*2"}, want: 23340},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calcLine(tt.args.line, tt.args.advancedMath); got != tt.want {
				t.Errorf("calcLine() = %v, want %v", got, tt.want)
			}
		})
	}
}
