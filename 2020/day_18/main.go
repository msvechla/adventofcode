package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 18: Operation Order")

	lines := parseInputFromFile()
	total := 0
	for _, l := range lines {
		total += calcLine(l, false)
	}
	fmt.Printf("Part1: %d (sum of the resulting expressions)\n", total)

	total = 0
	for _, l := range lines {
		total += calcLine(l, true)
	}
	fmt.Printf("Part2: %d (sum of the resulting expressions using advancedMath)\n", total)
}

func parseInputFromFile() []string {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInput(input)
}

func parseInput(input []byte) []string {
	lines := strings.Split(string(input), "\n")
	res := make([]string, len(lines)-1)

	for i, l := range lines {
		if l == "" {
			continue
		}
		res[i] = strings.ReplaceAll(l, " ", "")
	}
	return res
}

func calcLine(line string, advancedMath bool) int {
	plainExp := replaceParanthesisWithSolution(line, advancedMath)
	res := calculateExpression(plainExp, advancedMath)

	resInt, _ := strconv.Atoi(res)
	return resInt
}

func replaceParanthesisWithSolution(line string, advancedMath bool) string {
	r := regexp.MustCompile(`[\(\)]`)
	p := r.Find([]byte(line))
	if p == nil {
		return line
	}

	r = regexp.MustCompile(`\(([*+\s0-9]+)\)`)
	expInParanthesis := r.FindStringSubmatch(line)
	res := calculateExpression(expInParanthesis[1], advancedMath)

	return replaceParanthesisWithSolution(strings.Replace(line, expInParanthesis[0], res, 1), advancedMath)
}

func calculateExpression(exp string, advancedMath bool) string {
	r := regexp.MustCompile(`[\*\+]`)
	op := r.Find([]byte(exp))
	if op == nil {
		return exp
	}

	res := 0
	nextExp := []string{}
	if advancedMath {
		reNextExp := regexp.MustCompile(`(\d+)(\+)(\d+)`)
		nextExp = reNextExp.FindStringSubmatch(exp)
		if len(nextExp) != 4 {
			reNextExp := regexp.MustCompile(`(\d+)(\*)(\d+)`)
			nextExp = reNextExp.FindStringSubmatch(exp)
		}
		res = calc(nextExp[1], nextExp[2], nextExp[3])

	} else {
		reNextExp := regexp.MustCompile(`(\d+)([\+\*])(\d+)`)
		nextExp = reNextExp.FindStringSubmatch(exp)
		res = calc(nextExp[1], nextExp[2], nextExp[3])
	}

	return calculateExpression(strings.Replace(exp, nextExp[0], strconv.Itoa(res), 1), advancedMath)
}

func calc(a, op, b string) int {
	numA, _ := strconv.Atoi(a)
	numB, _ := strconv.Atoi(b)

	switch op {
	case "+":
		return numA + numB
	case "*":
		return numA * numB
	}
	return 0
}
