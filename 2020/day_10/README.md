# Advent of Code 2020 - Day 10: Adapter Array

- Puzzle: <https://adventofcode.com/2020/day/10>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

## Solution Approach for Part 2

- find numbers that can be exchanged / are optional
- loop over them and create groups of adjacent numbers that are optional
- count the occurence of each group of adjacent optional numbers

Solution: 

```go
2^(occGroupOneAdjacentOptNumbers) * 4^(occGroupTwoAdjacentOptNumbers) * 7^(occGroupThreeAdjacentOptNumbers)
```

**Valid Permutations:**

- 1 adjacent number has 2 valid permutations
- 2 adjacent numbers have 4 valid permutations
- 3 adjacent numbers have 7 valid permutations (000 is not valid)

