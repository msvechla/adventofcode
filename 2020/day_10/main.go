package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 10: Adapter Array")
	numbers := parseNumbersFromFile()
	diffs := getJoltDifferenceSum(numbers)
	fmt.Printf("Part1: %d (number of 1-jolt differences multiplied by the number of 3-jolt differences)\n", diffs[1]*diffs[3])

	possibilities := getNumberOfWaysToArrangeAdapters(numbers)
	fmt.Printf("Part2: %.0f (total number of distinct ways to arrange the adapters)\n", possibilities)
}

func getNumberOfWaysToArrangeAdapters(numbers []int) float64 {
	adjacent := getGroupsAndTheirOccurenceOfAdjacentOptionalNumbers(numbers)
	return math.Pow(2, float64(adjacent[1])) * math.Pow(4, float64(adjacent[2])) * math.Pow(7, float64(adjacent[3]))
}

func getGroupsAndTheirOccurenceOfAdjacentOptionalNumbers(numbers []int) map[int]int {
	diffs := getDiffsBetweenAdjacentNumbers(numbers)
	adjacentOnes := make(map[int]int, len(numbers)+1)

	consecutiveOnes := 0
	for i := 1; i < len(diffs); i++ {
		sumOfDiffs := diffs[i] + diffs[i-1]
		if sumOfDiffs <= 3 {
			consecutiveOnes++
		} else {
			if consecutiveOnes > 0 {
				adjacentOnes[consecutiveOnes]++
				consecutiveOnes = 0
			}
		}
	}

	return adjacentOnes
}

func getDiffsBetweenAdjacentNumbers(numbers []int) []int {
	sort.Ints(numbers)
	joltDiffs := make([]int, len(numbers)+1)

	// diff from outlet (0 jolts) to first adapter
	joltDiffs[0] = numbers[0]

	for i := 1; i < len(numbers); i++ {
		diff := numbers[i] - numbers[i-1]
		joltDiffs[i] = diff
	}

	// laptop itself has always diff of 3
	joltDiffs[len(numbers)] = 3

	return joltDiffs
}

func getJoltDifferenceSum(numbers []int) map[int]int {
	sort.Ints(numbers)
	joltDiffs := make(map[int]int, 3)

	// diff from outlet (0 jolts) to first adapter
	joltDiffs[numbers[0]]++

	for i := 1; i < len(numbers); i++ {
		diff := numbers[i] - numbers[i-1]
		joltDiffs[diff]++
	}

	// laptop itself has always diff of 3
	joltDiffs[3]++

	return joltDiffs
}

func parseNumbersFromFile() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseNumbers(input)
}

func parseNumbers(input []byte) []int {

	lines := strings.Split(string(input), "\n")
	numbers := make([]int, len(lines)-1)

	iCount := 0
	for _, l := range lines {
		if l == "" {
			continue
		}

		numbers[iCount], _ = strconv.Atoi(l)
		iCount++
	}
	return numbers
}
