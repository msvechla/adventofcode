package main

import (
	"reflect"
	"testing"
)

func Test_parseNumbersFromFile(t *testing.T) {
	tests := []struct {
		name string
		want []int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseNumbersFromFile(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseNumbersFromFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseNumbers(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "test", args: args{input: []byte(`
1
12
42`)}, want: []int{1, 12, 42}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseNumbers(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getJoltDifferenceSum(t *testing.T) {
	type args struct {
		numbers []int
	}
	tests := []struct {
		name string
		args args
		want map[int]int
	}{
		{name: "test", args: args{numbers: parseNumbers([]byte(`
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3`))}, want: map[int]int{1: 22, 3: 10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getJoltDifferenceSum(tt.args.numbers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getJoltDifferenceSum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getDiffsBetweenAdjacentNumbers(t *testing.T) {
	type args struct {
		numbers []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{name: "aocExample", args: args{numbers: parseNumbers([]byte(`
16
10
15
5
1
11
7
19
6
12
4`))}, want: []int{1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3, 3}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getDiffsBetweenAdjacentNumbers(tt.args.numbers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getDiffsBetweenAdjacentNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getGroupsAndTheirOccurenceOfAdjacentOptionalNumbers(t *testing.T) {
	type args struct {
		numbers []int
	}
	tests := []struct {
		name string
		args args
		want map[int]int
	}{
		{name: "aocExample", args: args{numbers: parseNumbers([]byte(`
16
10
15
5
1
11
7
19
6
12
4`))}, want: map[int]int{1: 1, 2: 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getGroupsAndTheirOccurenceOfAdjacentOptionalNumbers(tt.args.numbers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getGroupsAndTheirOccurenceOfAdjacentOptionalNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getNumberOfWaysToArrangeAdapters(t *testing.T) {
	type args struct {
		numbers []int
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "aocExample", args: args{numbers: parseNumbers([]byte(`
16
10
15
5
1
11
7
19
6
12
4`))}, want: 8},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getNumberOfWaysToArrangeAdapters(tt.args.numbers); got != tt.want {
				t.Errorf("getNumberOfWaysToArrangeAdapters() = %v, want %v", got, tt.want)
			}
		})
	}
}
