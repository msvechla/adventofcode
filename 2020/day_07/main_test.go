package main

import (
	"reflect"
	"testing"
)

func Test_parseRule(t *testing.T) {
	type args struct {
		rule string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 map[string]int
	}{
		{name: "1", args: args{rule: "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."}, want: "muted yellow", want1: map[string]int{"shiny gold": 2, "faded blue": 9}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := parseRule(tt.args.rule)
			if got != tt.want {
				t.Errorf("parseRule() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("parseRule() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_dependencyGraph_totalBagsWhichCanContainType(t *testing.T) {
	type args struct {
		bagType  string
		visisted map[string]bool
	}
	tests := []struct {
		name string
		d    dependencyGraph
		args args
		want int
	}{
		{name: "1", args: args{bagType: "shiny gold", visisted: map[string]bool{}}, d: generateDependencyGraph([]byte(`
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`)), want: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.totalBagsWhichCanContainType(tt.args.bagType, tt.args.visisted); got != tt.want {
				t.Errorf("dependencyGraph.totalBagsWhichCanContainType() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dependencyGraph_totalIndividualBagsRequiredInsideType(t *testing.T) {
	type args struct {
		bagType string
	}
	tests := []struct {
		name string
		d    dependencyGraph
		args args
		want int
	}{
		{name: "1", args: args{bagType: "shiny gold"}, d: generateGraph([]byte(`
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`)), want: 32},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.totalIndividualBagsRequiredInsideType(tt.args.bagType); got != tt.want {
				t.Errorf("dependencyGraph.totalIndividualBagsRequiredInsideType() = %v, want %v", got, tt.want)
			}
		})
	}
}
