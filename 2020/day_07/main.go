package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type dependencyGraph map[string]map[string]int

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 07: Handy Haversacks")
	deps := generateDependencyGraphFromFile()

	fmt.Printf("Part1: %v (total bags which contain 'shiny gold')\n", deps.totalBagsWhichCanContainType("shiny gold", map[string]bool{}))

	graph := generateGraphFromFile()
	fmt.Printf("Part2: %v (total individual bags required inside 'shiny gold')\n", graph.totalIndividualBagsRequiredInsideType("shiny gold"))
}

func (d dependencyGraph) totalBagsWhichCanContainType(bagType string, visisted map[string]bool) int {
	sum := 0
	for bag, _ := range d[bagType] {
		if _, alreadyVisisted := visisted[bag]; alreadyVisisted {
			continue
		}
		visisted[bag] = true
		sum++
		sum += d.totalBagsWhichCanContainType(bag, visisted)
	}
	return sum
}

func (d dependencyGraph) totalIndividualBagsRequiredInsideType(bagType string) int {
	total := 0
	for bag, quantity := range d[bagType] {
		total += quantity
		total += quantity * d.totalIndividualBagsRequiredInsideType(bag)
	}
	return total
}

func generateDependencyGraphFromFile() dependencyGraph {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return generateDependencyGraph(input)
}

func generateDependencyGraph(input []byte) dependencyGraph {

	lines := strings.Split(string(input), "\n")
	dependencies := make(map[string]map[string]int, len(lines)-1)

	for _, l := range lines {
		if l == "" {
			continue
		}
		bag, contains := parseRule(l)
		for b, q := range contains {
			if _, exists := dependencies[b]; !exists {
				dependencies[b] = map[string]int{}
			}
			dependencies[b][bag] = q
		}

	}
	return dependencies
}

func generateGraphFromFile() dependencyGraph {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return generateGraph(input)
}

func generateGraph(input []byte) dependencyGraph {

	lines := strings.Split(string(input), "\n")
	dependencies := make(map[string]map[string]int, len(lines)-1)

	for _, l := range lines {
		if l == "" {
			continue
		}
		bag, contains := parseRule(l)
		dependencies[bag] = contains
	}
	return dependencies
}

func parseRule(rule string) (string, map[string]int) {
	parts := strings.Split(rule, " bags contain")
	parent := parts[0]

	re := regexp.MustCompile(`[,.]?\s(\d)\s(\w+\s\w+)\sbag[s]?`)
	matches := re.FindAllStringSubmatch(parts[1], -1)

	contains := make(map[string]int, len(matches))

	for _, m := range matches {
		quantity, _ := strconv.Atoi(m[1])
		bagType := m[2]

		contains[bagType] = quantity
	}

	return parent, contains
}
