# Advent of Code 2020 - Day 7: Handy Haversacks

- Puzzle: <https://adventofcode.com/2020/day/7>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

