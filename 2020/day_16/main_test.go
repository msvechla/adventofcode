package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parseRule(t *testing.T) {
	type args struct {
		rule string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 []numRange
	}{
		{name: "1", args: args{rule: "departure location: 41-526 or 547-973"}, want: "departure location", want1: []numRange{numRange{min: 41, max: 526}, numRange{min: 547, max: 973}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := parseRule(tt.args.rule)
			assert.Equal(t, tt.want, got)
			assert.ElementsMatch(t, tt.want1, got1)
		})
	}
}

func Test_parseTicket(t *testing.T) {
	type args struct {
		ticket string
	}
	tests := []struct {
		name string
		args args
		want ticket
	}{
		{name: "1", args: args{ticket: "7,1,14"}, want: ticket{7, 1, 14}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseTicket(tt.args.ticket); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseTicket() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ticket_isValid(t *testing.T) {
	type args struct {
		rules ticketRules
	}
	tests := []struct {
		name  string
		t     ticket
		args  args
		want  bool
		want1 int
	}{
		{name: "aocExample", args: args{rules: ticketRules{
			"class": []numRange{
				numRange{min: 1, max: 3},
				numRange{min: 5, max: 7}},
			"row": []numRange{
				numRange{min: 6, max: 11},
				numRange{min: 33, max: 44}},
			"seat": []numRange{
				numRange{min: 13, max: 40},
				numRange{min: 45, max: 50}},
		},
		}, t: ticket{40, 4, 50}, want: false, want1: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.t.isValid(tt.args.rules)
			if got != tt.want {
				t.Errorf("ticket.isValid() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ticket.isValid() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_parseInput(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name  string
		args  args
		want  ticketRules
		want1 ticket
		want2 []ticket
	}{
		{name: "aocExample", args: args{input: []byte(`
class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12`)}, want: ticketRules{
			"class": []numRange{
				numRange{min: 1, max: 3},
				numRange{min: 5, max: 7}},
			"row": []numRange{
				numRange{min: 6, max: 11},
				numRange{min: 33, max: 44}},
			"seat": []numRange{
				numRange{min: 13, max: 40},
				numRange{min: 45, max: 50}},
		},
			want1: ticket{7, 1, 14},
			want2: []ticket{
				{7, 3, 47},
				{40, 4, 50},
				{55, 2, 20},
				{38, 6, 12},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2 := parseInput(tt.args.input)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseInput() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("parseInput() got1 = %v, want %v", got1, tt.want1)
			}
			if !reflect.DeepEqual(got2, tt.want2) {
				t.Errorf("parseInput() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

func Test_ticketRules_identifyFieldNameForPosition(t *testing.T) {
	rules, _, tickts := parseInput([]byte(`
class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9`))

	assignments := rules.identifyFieldNames(tickts)
	assert.Equal(t, map[int]string{0: "row", 1: "class", 2: "seat"}, assignments)
}
