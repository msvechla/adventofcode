package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type numRange struct {
	min, max int
}

type ticketRules map[string][]numRange

type ticket []int

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 16: Ticket Translation")
	rules, myTicket, nearbyTickets := parseInputFromFile()

	errorRate := 0
	validTickets := []ticket{}
	for _, t := range nearbyTickets {
		valid, n := t.isValid(rules)

		if !valid {
			errorRate += n
			continue
		}
		validTickets = append(validTickets, t)
	}
	fmt.Printf("Part1: %d (error rate for nearby tickets)\n", errorRate)

	assignments := rules.identifyFieldNames(validTickets)
	res := 1
	for pos, val := range myTicket {
		if strings.Contains(assignments[pos], "departure") {
			res *= val
		}
	}

	fmt.Printf("Part2: %d (product of all fields in my ticket containing 'departure')\n", res)
}

func (r ticketRules) identifyFieldNames(tickets []ticket) map[int]string {
	possiblePosForFields := map[int][]string{}
	for pos := 0; pos < len(tickets[0]); pos++ {
		for name, rule := range r {
			ruleMatches := false
			for _, t := range tickets {
				ticketPos := t[pos : pos+1]
				ruleMatches, _ = ticketPos.isValid(ticketRules{name: rule})
				if !ruleMatches {
					break
				}
			}
			if ruleMatches {
				possiblePosForFields[pos] = append(possiblePosForFields[pos], name)
			}
		}
	}

	finalFields := map[int]string{}
	for len(possiblePosForFields) >= 1 {
		for fieldPos, possibleFieldNames := range possiblePosForFields {
			remMatches := []string{}
			for _, possibleFieldName := range possibleFieldNames {
				remove := false
				for _, takenFieldNames := range finalFields {
					if possibleFieldName == takenFieldNames {
						remove = true
					}
				}
				if !remove {
					remMatches = append(remMatches, possibleFieldName)
				}
			}
			possiblePosForFields[fieldPos] = remMatches
			if len(remMatches) == 1 {
				finalFields[fieldPos] = remMatches[0]
				delete(possiblePosForFields, fieldPos)
			}
		}
	}
	for pos, field := range possiblePosForFields {
		finalFields[pos] = field[0]
	}
	return finalFields
}

func parseInputFromFile() (ticketRules, ticket, []ticket) {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseInput(input)
}

func parseInput(input []byte) (ticketRules, ticket, []ticket) {
	lines := strings.Split(string(input), "\n")
	tr := ticketRules{}
	myTicket := ticket{}
	nearbyTickets := []ticket{}

	lastLine := 0
	for i, l := range lines {
		lastLine = i
		if l == "" {
			continue
		}

		if strings.Contains(l, "your ticket") {
			break
		}
		name, rule := parseRule(l)
		if name == "" {
			continue
		}
		tr[name] = rule
	}

	myTicket = parseTicket(lines[lastLine+1])

	for i := lastLine + 4; i < len(lines); i++ {
		if lines[i] == "" {
			continue
		}
		t := parseTicket(lines[i])
		nearbyTickets = append(nearbyTickets, t)
	}

	return tr, myTicket, nearbyTickets
}

func (t ticket) isValid(rules ticketRules) (bool, int) {
	for _, n := range t {
		valid := false
		for _, r := range rules {
			if valid {
				break
			}
			for _, tr := range r {
				if n >= tr.min && n <= tr.max {
					valid = true
					break
				}
			}
		}
		if !valid {
			return false, n
		}
	}
	return true, 0
}

func parseRule(rule string) (string, []numRange) {
	r := regexp.MustCompile(`(.*):\s(\d+)-(\d+)\sor\s(\d+)-(\d+)`)
	matches := r.FindStringSubmatch(rule)

	if len(matches) != 6 {
		return "", nil
	}

	min1, _ := strconv.Atoi(matches[2])
	max1, _ := strconv.Atoi(matches[3])
	min2, _ := strconv.Atoi(matches[4])
	max2, _ := strconv.Atoi(matches[5])

	return matches[1], []numRange{numRange{min: min1, max: max1}, numRange{min: min2, max: max2}}
}

func parseTicket(ticket string) ticket {
	numString := strings.Split(ticket, ",")
	nums := make([]int, len(numString))

	for i, n := range numString {
		num, _ := strconv.Atoi(n)
		nums[i] = num
	}

	return nums
}
