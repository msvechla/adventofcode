package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type memory map[int][]int

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 15: Rambunctious Recitation")

	nums := parseNumbersFromFile()
	m := memory{}
	fmt.Printf("Part1: %d (%dth number spoken)\n", m.play(nums, 2020), 2020)
	m = memory{}
	fmt.Printf("Part2: %d (%dth number spoken)\n", m.play(nums, 30000000), 30000000)
}

func (m memory) play(numbers []int, turns int) int {
	currentNum := 0
	lastNum := 0
	for turn := 1; turn <= turns; turn++ {
		if turn <= len(numbers) {
			currentNum = numbers[turn-1]
			m[currentNum] = []int{turn}
			lastNum = currentNum
			continue
		}
		if len(m[lastNum]) <= 1 {
			currentNum = 0
			m.add(currentNum, turn)
			lastNum = currentNum
			continue
		}
		currentNum = m[lastNum][0] - m[lastNum][1]
		m.add(currentNum, turn)
		lastNum = currentNum
	}
	return currentNum
}

func (m memory) add(number, turn int) {
	if _, exists := m[number]; !exists {
		m[number] = []int{turn}
		return
	}

	newNums := make([]int, 2)
	newNums[1] = m[number][0]
	newNums[0] = turn

	m[number] = newNums
}

func parseNumbersFromFile() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseNumbers(input)
}

func parseNumbers(input []byte) []int {

	lines := strings.Split(string(input), ",")
	numbers := make([]int, len(lines))

	iCount := 0
	for _, l := range lines {
		if l == "" {
			continue
		}

		numbers[iCount], _ = strconv.Atoi(l)
		iCount++
	}
	return numbers
}
