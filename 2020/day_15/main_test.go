package main

import "testing"

func Test_memory_play(t *testing.T) {
	type args struct {
		numbers []int
		turns   int
	}
	tests := []struct {
		name string
		m    memory
		args args
		want int
	}{
		{name: "aocExample", args: args{numbers: parseNumbers([]byte("0,3,6")), turns: 2020}, m: memory{}, want: 436},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.play(tt.args.numbers, tt.args.turns); got != tt.want {
				t.Errorf("memory.play() = %v, want %v", got, tt.want)
			}
		})
	}
}
