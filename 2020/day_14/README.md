# Advent of Code 2020 - Day 14: Docking Data

- Puzzle: <https://adventofcode.com/2020/day/14>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

