package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
)

type maskOperation struct {
	maskBitsOverride map[int]int
	operations       []op
}

type op struct {
	address int
	value   []rune
}

type floatMaskOperation struct {
	maskBitsOverride map[int]rune
	operations       []floatOp
}

type floatOp struct {
	address []rune
	value   int
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 14: Docking Data")
	maskOps := parseMaskOperationsFromFile()

	mem := runMaskOperations(maskOps)

	var total int64 = 0
	for _, val := range mem {
		total += val
	}

	fmt.Printf("Part1: %d (sum of all values left in memory after completion)\n", total)
	floatMaskOps := parseFloatMaskOperationsFromFile()

	floatMem := runFloatMaskOperations(floatMaskOps)

	totalFloat := 0
	for _, val := range floatMem {
		totalFloat += val
	}

	fmt.Printf("Part2: %d (sum of all values left in memory after completion via floating calculation)\n", totalFloat)
}

func runFloatMaskOperations(ops []floatMaskOperation) map[int64]int {
	mem := map[int64]int{}

	for _, maskOp := range ops {
		for _, op := range maskOp.operations {
			op.applyFloatMask(maskOp.maskBitsOverride)
			addrs := generatePossibleMemAddresses(string(op.address))

			for _, a := range addrs {
				mem[a] = op.value
			}
		}
	}
	return mem
}

func runMaskOperations(ops []maskOperation) map[int]int64 {
	mem := map[int]int64{}

	for _, maskOp := range ops {
		for _, op := range maskOp.operations {
			op.applyMask(maskOp.maskBitsOverride)
			i, _ := strconv.ParseInt(string(op.value), 2, 64)
			mem[op.address] = i
		}
	}
	return mem
}

func (o *floatOp) applyFloatMask(mask map[int]rune) []rune {
	for bit, val := range mask {
		o.address[bit] = val
	}
	return o.address
}

func (o *op) applyMask(mask map[int]int) []rune {
	for bit, val := range mask {
		o.value[bit] = []rune(strconv.Itoa(val))[0]
	}
	return o.value
}

func parseMaskOperationsFromFile() []maskOperation {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseMaskOperations(input)
}

func parseFloatMaskOperationsFromFile() []floatMaskOperation {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseFloatMaskOperations(input)
}

func generatePossibleMemAddresses(floatAddress string) []int64 {
	numPossibilites := math.Pow(2, float64(strings.Count(floatAddress, "X")))
	memAddresses := []int64{}

	if numPossibilites == 0 {
		return []int64{}
	}

	for i := 0; i < int(numPossibilites); i++ {
		floatingBits := []rune(fmt.Sprintf("%036b", i))
		newAddr := []rune(floatAddress)

		replaced := 0
		for i, r := range floatAddress {
			if r != 'X' {
				continue
			}
			newAddr[i] = floatingBits[len(floatingBits)-1-replaced]
			replaced++
		}
		newAddrInt, _ := strconv.ParseInt(string(newAddr), 2, 64)
		memAddresses = append(memAddresses, newAddrInt)
	}
	return memAddresses
}

func parseFloatMaskOperations(input []byte) []floatMaskOperation {

	lines := strings.Split(string(input), "\n")
	maskOps := []floatMaskOperation{}

	currentOps := []floatOp{}
	currentMask := map[int]rune{}
	for _, l := range lines {
		if l == "" {
			continue
		}
		if strings.HasPrefix(l, "mask") {
			if len(currentMask) > 0 {
				maskOps = append(maskOps, floatMaskOperation{maskBitsOverride: currentMask, operations: currentOps})
			}
			currentOps = []floatOp{}
			currentMask = parseFloatMask(strings.Trim(l, "mask = "))
		} else {
			currentOps = append(currentOps, parseFloatOp(l))
		}
	}

	maskOps = append(maskOps, floatMaskOperation{maskBitsOverride: currentMask, operations: currentOps})

	return maskOps
}

func parseMaskOperations(input []byte) []maskOperation {

	lines := strings.Split(string(input), "\n")
	maskOps := []maskOperation{}

	currentOps := []op{}
	currentMask := map[int]int{}
	for _, l := range lines {
		if l == "" {
			continue
		}
		if strings.HasPrefix(l, "mask") {
			if len(currentMask) > 0 {
				maskOps = append(maskOps, maskOperation{maskBitsOverride: currentMask, operations: currentOps})
			}
			currentOps = []op{}
			currentMask = parseMask(strings.Trim(l, "mask = "))
		} else {
			currentOps = append(currentOps, parseOp(l))
		}
	}

	maskOps = append(maskOps, maskOperation{maskBitsOverride: currentMask, operations: currentOps})

	return maskOps
}

func parseMask(mask string) map[int]int {
	m := make(map[int]int, len(mask)-strings.Count(mask, "X"))
	for i, b := range mask {
		if b == 'X' {
			continue
		}
		m[i], _ = strconv.Atoi(string(b))
	}
	return m
}

func parseOp(operation string) op {
	r := regexp.MustCompile(`mem\[(\d+)\]\s=\s+(\d+)`)
	matches := r.FindStringSubmatch(operation)
	if len(matches) != 3 {
		return op{}
	}
	address, _ := strconv.Atoi(matches[1])
	value, _ := strconv.Atoi(matches[2])
	binValString := fmt.Sprintf("%036b", value)
	return op{address: address, value: []rune(binValString)}
}

func parseFloatMask(mask string) map[int]rune {
	m := make(map[int]rune, len(mask)-strings.Count(mask, "0"))
	for i, b := range mask {
		if b == '0' {
			continue
		}
		m[i] = b
	}
	return m
}

func parseFloatOp(operation string) floatOp {
	r := regexp.MustCompile(`mem\[(\d+)\]\s=\s+(\d+)`)
	matches := r.FindStringSubmatch(operation)
	if len(matches) != 3 {
		return floatOp{}
	}
	address, _ := strconv.Atoi(matches[1])
	binAddrString := fmt.Sprintf("%036b", address)
	value, _ := strconv.Atoi(matches[2])
	return floatOp{address: []rune(binAddrString), value: value}
}
