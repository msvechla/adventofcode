package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parseMaskOperationsFromFile(t *testing.T) {
	tests := []struct {
		name string
		want []maskOperation
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseMaskOperationsFromFile(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseMaskOperationsFromFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseMaskOperations(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []maskOperation
	}{
		{name: "aocExample", args: args{input: []byte(`
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0`)}, want: []maskOperation{maskOperation{maskBitsOverride: map[int]int{34: 0, 29: 1}, operations: []op{{address: 8, value: []rune("000000000000000000000000000000001011")}, {address: 7, value: []rune("000000000000000000000000000001100101")}, {address: 8, value: []rune("000000000000000000000000000000000000")}}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseMaskOperations(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseMaskOperations() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseMask(t *testing.T) {
	type args struct {
		mask string
	}
	tests := []struct {
		name string
		args args
		want map[int]int
	}{
		{name: "aocExample", args: args{mask: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"}, want: map[int]int{34: 0, 29: 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseMask(tt.args.mask); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseMask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseOp(t *testing.T) {
	type args struct {
		operation string
	}
	tests := []struct {
		name string
		args args
		want op
	}{
		{name: "aocExample", args: args{operation: "mem[8] = 11"}, want: op{address: 8, value: []rune("000000000000000000000000000000001011")}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseOp(tt.args.operation); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseOp() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_op_applyMask(t *testing.T) {
	type fields struct {
		address int
		value   []rune
	}
	type args struct {
		mask map[int]int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []rune
	}{
		{name: "aocExample", args: args{mask: parseMask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")}, fields: fields{address: 8, value: []rune("000000000000000000000000000000001011")}, want: []rune("000000000000000000000000000001001001")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &op{
				address: tt.fields.address,
				value:   tt.fields.value,
			}
			if got := o.applyMask(tt.args.mask); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("op.applyMask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseFloatMaskOperations(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want []floatMaskOperation
	}{
		{name: "aocExample", args: args{input: []byte(`
mask = 000000000000000000000000000000X1001X
mem[42] = 100`)}, want: []floatMaskOperation{floatMaskOperation{maskBitsOverride: map[int]rune{35: 'X', 34: '1', 31: '1', 30: 'X'}, operations: []floatOp{{address: []rune("000000000000000000000000000000101010"), value: 100}}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseFloatMaskOperations(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseFloatMaskOperations() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_generatePossibleMemAddresses(t *testing.T) {
	type args struct {
		floatAddress string
	}
	tests := []struct {
		name string
		args args
		want []int64
	}{
		{name: "aocExample", args: args{floatAddress: "000000000000000000000000000000X1101X"}, want: []int64{26, 27, 58, 59}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.ElementsMatch(t, generatePossibleMemAddresses(tt.args.floatAddress), tt.want)
		})
	}
}
