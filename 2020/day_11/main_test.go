package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_floorPlan_getAdjacentOccupiedSeats(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		fp   floorPlan
		args args
		want [][]int
	}{
		{name: "test", args: args{x: 1, y: 1}, fp: parseFloorPlan([]byte(`
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`)), want: [][]int{}},
		{name: "test 2", args: args{x: 1, y: 1}, fp: parseFloorPlan([]byte(`
L#LL.LL.LL
LLLLLLL.LL
L#L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`)), want: [][]int{{1, 0}, {1, 2}}},
		{name: "test 3", args: args{x: 0, y: 1}, fp: parseFloorPlan([]byte(`
L#LL.LL.LL
LLLLLLL.LL
L#L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`)), want: [][]int{{1, 0}, {1, 2}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fp.getAdjacentOccupiedSeats(tt.args.x, tt.args.y); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("floorPlan.getAdjacentOccupiedSeats() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_floorPlan_modelArrivingPeople(t *testing.T) {
	tests := []struct {
		name  string
		fp    floorPlan
		want  bool
		want1 floorPlan
	}{
		{name: "aocExample", fp: parseFloorPlan([]byte(`
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`)), want: true, want1: parseFloorPlan([]byte(`
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##`))},
		{name: "aocExample Step 2", fp: parseFloorPlan([]byte(`
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##`)), want: true, want1: parseFloorPlan([]byte(`
#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##`))},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.fp.modelArrivingPeople()
			if got != tt.want {
				t.Errorf("floorPlan.modelArrivingPeople() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("floorPlan.modelArrivingPeople() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_floorPlan_totalOccupiedSeats(t *testing.T) {
	tests := []struct {
		name string
		fp   floorPlan
		want int
	}{
		{name: "test", fp: parseFloorPlan([]byte(`
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.L#.#L.LL
L.LLLLL.LL
..L.L.....
LLLLLLL#LL
L.LLLLLL.L
L.LLLLL.LL`)), want: 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fp.totalOccupiedSeats(); got != tt.want {
				t.Errorf("floorPlan.totalOccupiedSeats() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_floorPlan_getNextSeat(t *testing.T) {
	type args struct {
		x         int
		y         int
		direction string
	}
	tests := []struct {
		name  string
		fp    floorPlan
		args  args
		want  []int
		want1 rune
	}{
		{name: "test", args: args{x: 3, y: 4, direction: "S"}, fp: parseFloorPlan([]byte(`
.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#.....`)), want: []int{3, 8}, want1: TypeOccupiedSeat},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pos, seatType := tt.fp.getNextSeat(tt.args.x, tt.args.y, tt.args.direction)

			assert.Equal(t, tt.want, pos)
			assert.Equal(t, tt.want1, seatType)
		})
	}
}

func Test_floorPlan_getAdjacentVisibleOccupiedSeats(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		fp   floorPlan
		args args
		want [][]int
	}{
		{name: "aocExample", args: args{x: 1, y: 1}, fp: parseFloorPlan([]byte(`
.............
.L...#.#.#.#.
.............`)), want: [][]int{{5, 1}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fp.getAdjacentVisibleOccupiedSeats(tt.args.x, tt.args.y); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("floorPlan.getAdjacentVisibleOccupiedSeats() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_floorPlan_modelArrivingPeopleVisual(t *testing.T) {
	tests := []struct {
		name  string
		fp    floorPlan
		want  bool
		want1 floorPlan
	}{
		{name: "aocExample Step 1", fp: parseFloorPlan([]byte(`
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`)), want: true, want1: parseFloorPlan([]byte(`
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##`))},
		{name: "aocExample Step 2", fp: parseFloorPlan([]byte(`
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##`)), want: true, want1: parseFloorPlan([]byte(`
#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#`))},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.fp.modelArrivingPeopleVisual()
			if got != tt.want {
				t.Errorf("floorPlan.modelArrivingPeopleVisual() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("floorPlan.modelArrivingPeopleVisual() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
