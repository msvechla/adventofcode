package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type floorPlan [][]rune

const (
	TypeFloor        = '.'
	TypeEmptySeat    = 'L'
	TypeOccupiedSeat = '#'
)

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 11: Seating System")
	fp := parseFloorPlanFromFile()
	for {
		changed, _ := fp.modelArrivingPeople()
		if !changed {
			break
		}
	}
	fmt.Printf("Part1: %d (occupied seats after chaos stabilizes)\n", fp.totalOccupiedSeats())

	fp = parseFloorPlanFromFile()
	for {
		changed, _ := fp.modelArrivingPeopleVisual()
		if !changed {
			break
		}
	}
	fmt.Printf("Part2: %d (occupied seats visual appraoch after chaos stabilizes)\n", fp.totalOccupiedSeats())
}

func parseFloorPlanFromFile() floorPlan {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return parseFloorPlan(input)
}

func parseFloorPlan(input []byte) floorPlan {

	lines := strings.Split(string(input), "\n")
	fp := make([][]rune, len(lines)-1)

	countRows := 0
	for _, l := range lines {
		if l == "" {
			continue
		}
		fp[countRows] = []rune(l)
		countRows++
	}
	return fp
}

func (fp floorPlan) totalOccupiedSeats() int {
	total := 0
	for _, row := range fp {
		total += strings.Count(string(row), string(TypeOccupiedSeat))
	}
	return total
}

func (fp floorPlan) modelArrivingPeopleVisual() (bool, floorPlan) {
	changed := false
	deepCopy := floorPlan(make([][]rune, fp.height()))

	for i := range fp {
		deepCopy[i] = make([]rune, len(fp[i]))
		copy(deepCopy[i], fp[i])
	}

	for x := 0; x < fp.width(); x++ {
		for y := 0; y < fp.height(); y++ {
			if deepCopy[y][x] == TypeEmptySeat {
				if len(deepCopy.getAdjacentVisibleOccupiedSeats(x, y)) == 0 {
					fp[y][x] = TypeOccupiedSeat
					changed = true
				}
			}
			if deepCopy[y][x] == TypeOccupiedSeat {
				if len(deepCopy.getAdjacentVisibleOccupiedSeats(x, y)) >= 5 {
					fp[y][x] = TypeEmptySeat
					changed = true
				}
			}
		}
	}
	return changed, fp
}

func (fp floorPlan) modelArrivingPeople() (bool, floorPlan) {
	changed := false
	deepCopy := floorPlan(make([][]rune, fp.height()))

	for i := range fp {
		deepCopy[i] = make([]rune, len(fp[i]))
		copy(deepCopy[i], fp[i])
	}

	for x := 0; x < fp.width(); x++ {
		for y := 0; y < fp.height(); y++ {
			if deepCopy[y][x] == TypeEmptySeat {
				if len(deepCopy.getAdjacentOccupiedSeats(x, y)) == 0 {
					fp[y][x] = TypeOccupiedSeat
					changed = true
				}
			}
			if deepCopy[y][x] == TypeOccupiedSeat {
				if len(deepCopy.getAdjacentOccupiedSeats(x, y)) >= 4 {
					fp[y][x] = TypeEmptySeat
					changed = true
				}
			}
		}
	}
	return changed, fp
}

func (fp floorPlan) width() int {
	return len(fp[0])
}
func (fp floorPlan) height() int {
	return len(fp)
}

func (fp floorPlan) getNextSeat(x, y int, direction string) ([]int, rune) {
	cX := 0
	cY := 0

	if strings.Contains(direction, "N") {
		cY = -1
	}
	if strings.Contains(direction, "S") {
		cY = 1
	}
	if strings.Contains(direction, "E") {
		cX = 1
	}
	if strings.Contains(direction, "W") {
		cX = -1
	}

	if x+cX < 0 || x+cX >= fp.width() {
		return []int{}, 'x'
	}
	if y+cY < 0 || y+cY >= fp.height() {
		return []int{}, 'x'
	}
	if fp[y+cY][x+cX] == TypeOccupiedSeat || fp[y+cY][x+cX] == TypeEmptySeat {
		return []int{x + cX, y + cY}, fp[y+cY][x+cX]
	}
	return fp.getNextSeat(x+cX, y+cY, direction)
}

func (fp floorPlan) getAdjacentVisibleOccupiedSeats(x, y int) [][]int {
	seats := [][]int{}
	directions := []string{"N", "S", "E", "W", "NE", "NW", "SE", "SW"}
	for _, d := range directions {
		pos, seatType := fp.getNextSeat(x, y, d)
		if len(pos) > 0 && seatType == TypeOccupiedSeat {
			seats = append(seats, []int{pos[0], pos[1]})
		}
	}
	return seats
}

func (fp floorPlan) getAdjacentOccupiedSeats(x, y int) [][]int {
	seats := [][]int{}
	posModifier := []int{0, -1, 1}
	for _, cX := range posModifier {
		for _, cY := range posModifier {
			if cX == 0 && cY == 0 {
				continue
			}
			if x+cX < 0 || x+cX >= fp.width() {
				continue
			}
			if y+cY < 0 || y+cY >= fp.height() {
				continue
			}
			if fp[y+cY][x+cX] == TypeOccupiedSeat {
				seats = append(seats, []int{x + cX, y + cY})
			}
		}

	}
	return seats
}

func (fp floorPlan) String() string {
	var sb strings.Builder
	for _, row := range fp {
		sb.WriteString(fmt.Sprintf("%s\n", string(row)))
	}
	return sb.String()
}
