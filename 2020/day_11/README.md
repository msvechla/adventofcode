# Advent of Code 2020 - Day 11: Seating System

- Puzzle: <https://adventofcode.com/2020/day/11>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

