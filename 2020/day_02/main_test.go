package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parsePasswordLine(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want *password
	}{
		{name: "1", args: args{input: "4-8 n: dnjjrtclnzdnghnbnn"}, want: &password{minOcc: 4, maxOcc: 8, reqChar: []rune("n")[0], value: "dnjjrtclnzdnghnbnn"}},
		{name: "2", args: args{input: "2-18 x: wasd"}, want: &password{minOcc: 2, maxOcc: 18, reqChar: []rune("x")[0], value: "wasd"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parsePasswordLine(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parsePasswordLine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_password_isValid(t *testing.T) {
	type fields struct {
		reqChar rune
		minOcc  int
		maxOcc  int
		value   string
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{name: "1", fields: fields{reqChar: []rune("a")[0], minOcc: 1, maxOcc: 3, value: "abcde"}, want: true},
		{name: "2", fields: fields{reqChar: []rune("b")[0], minOcc: 1, maxOcc: 3, value: "cdefg"}, want: false},
		{name: "3", fields: fields{reqChar: []rune("c")[0], minOcc: 2, maxOcc: 9, value: "ccccccccc"}, want: true},
		{name: "4", fields: fields{reqChar: []rune("c")[0], minOcc: 2, maxOcc: 8, value: "ccccccccc"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &password{
				reqChar: tt.fields.reqChar,
				minOcc:  tt.fields.minOcc,
				maxOcc:  tt.fields.maxOcc,
				value:   tt.fields.value,
			}
			if got := p.isValid(); got != tt.want {
				t.Errorf("password.isValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_password_isValidOfficial(t *testing.T) {
	type fields struct {
		reqChar rune
		minOcc  int
		maxOcc  int
		value   string
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{name: "1", fields: fields{reqChar: []rune("a")[0], minOcc: 1, maxOcc: 3, value: "abcde"}, want: true},
		{name: "2", fields: fields{reqChar: []rune("b")[0], minOcc: 1, maxOcc: 3, value: "cdefg"}, want: false},
		{name: "3", fields: fields{reqChar: []rune("c")[0], minOcc: 2, maxOcc: 9, value: "ccccccccc"}, want: false},
		{name: "4", fields: fields{reqChar: []rune("c")[0], minOcc: 2, maxOcc: 8, value: "ccccccccc"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &password{
				reqChar: tt.fields.reqChar,
				minOcc:  tt.fields.minOcc,
				maxOcc:  tt.fields.maxOcc,
				value:   tt.fields.value,
			}
			if got := p.isValidOfficial(); got != tt.want {
				t.Errorf("password.isValidOfficial() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parsePasswordFile(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.True(t, len(parsePasswordFile()) > 100)
		})
	}
}
