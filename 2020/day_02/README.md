# Advent of Code 2020 - Day 2: Password Philosophy

- Puzzle: <https://adventofcode.com/2020/day/2>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

