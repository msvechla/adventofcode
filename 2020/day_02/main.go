package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type password struct {
	reqChar rune
	minOcc  int
	maxOcc  int
	value   string
}

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 02: Password Philosophy")
	passwords := parsePasswordFile()

	validPWCount := 0

	for _, p := range passwords {
		if p.isValid() {
			validPWCount++
		}
	}

	fmt.Printf("Part1: %d (valid passwords)\n", validPWCount)

	validPWCountOfficial := 0

	for _, p := range passwords {
		if p.isValidOfficial() {
			validPWCountOfficial++
		}
	}

	fmt.Printf("Part2: %d (valid passwords)\n", validPWCountOfficial)
}

func parsePasswordFile() []password {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")

	passwords := make([]password, len(lines)-1)
	for i, l := range lines {
		if l == "" {
			continue
		}
		p := parsePasswordLine(l)

		if p != nil {
			passwords[i] = *p
		}
	}
	return passwords
}

func parsePasswordLine(input string) *password {
	re := regexp.MustCompile(`(\d+)-(\d+)\s(.):\s(.*)`)

	matches := re.FindStringSubmatch(input)
	if len(matches) != 5 {
		return nil
	}

	min, err := strconv.Atoi(matches[1])
	if err != nil {
		return nil
	}

	max, err := strconv.Atoi(matches[2])
	if err != nil {
		return nil
	}

	return &password{
		minOcc:  min,
		maxOcc:  max,
		reqChar: []rune(matches[3])[0],
		value:   matches[4],
	}
}

func (p *password) isValid() bool {
	occ := 0
	for _, c := range p.value {
		if c == p.reqChar {
			occ++
		}
	}

	if occ < p.minOcc || occ > p.maxOcc {
		return false
	}

	return true
}

func (p *password) isValidOfficial() bool {
	runes := []rune(p.value)
	leftPosMatch := runes[p.minOcc-1] == p.reqChar

	rightPosMatch := false
	if p.maxOcc <= len(runes) && runes[p.maxOcc-1] == p.reqChar {
		rightPosMatch = true
	}

	return (leftPosMatch != rightPosMatch) && (leftPosMatch || rightPosMatch)
}
