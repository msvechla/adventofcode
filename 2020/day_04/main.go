package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type passport map[string]string

func main() {
	fmt.Println("\nAdvent of Code 2020 - Day 04: Passport Processing")

	passports := parsePassports()

	// part 1
	validPassports := 0
	for _, p := range passports {
		if p.isValid() {
			validPassports++
		}
	}

	fmt.Printf("Part1: %d (valid passports)\n", validPassports)

	// part 2
	validPassports = 0
	for _, p := range passports {
		if p.isValidStrict() {
			validPassports++
		}
	}

	fmt.Printf("Part2: %d (valid passports)\n", validPassports)
}

func parsePassports() []passport {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")

	passports := make([]passport, len(lines)-1)
	countPassports := 0
	currentPassport := passport{}

	for _, l := range lines {
		if l == "" {
			passports[countPassports] = currentPassport
			currentPassport = passport{}
			countPassports++
			continue
		}
		fields := strings.Split(l, " ")
		for _, f := range fields {
			kv := strings.Split(f, ":")
			currentPassport[kv[0]] = kv[1]
		}
	}
	return passports[:countPassports]
}

func (p passport) isValid() bool {
	if len(p) == 8 {
		return true
	}

	_, cidExists := p["cid"]
	if len(p) == 7 && !cidExists {
		return true
	}

	return false
}

func (p passport) isValidStrict() bool {
	if len(p) < 7 {
		return false
	}

	_, cidExists := p["cid"]
	if len(p) == 7 && cidExists {
		return false
	}

	return isValidNumber(p["byr"], 4, 1920, 2002) &&
		isValidNumber(p["iyr"], 4, 2010, 2020) &&
		isValidNumber(p["eyr"], 4, 2020, 2030) &&
		isValidHGT(p["hgt"]) &&
		isValidHCL(p["hcl"]) &&
		isValidECL(p["ecl"]) &&
		isValidNumber(p["pid"], 9, 0, 999999999)
}

func isValidNumber(num string, length, min, max int) bool {
	if length != 0 && len(num) != length {
		return false
	}
	y, err := strconv.Atoi(num)
	if err != nil {
		return false
	}
	return y >= min && y <= max
}

func isValidHGT(hgt string) bool {
	unit := ""
	if strings.HasSuffix(hgt, "cm") {
		unit = "cm"
	}
	if strings.HasSuffix(hgt, "in") {
		unit = "in"
	}
	if unit == "" {
		return false
	}

	val := strings.Trim(hgt, unit)
	if unit == "cm" && !isValidNumber(val, 0, 150, 193) {
		return false
	}
	if unit == "in" && !isValidNumber(val, 0, 59, 76) {
		return false
	}
	return true
}

func isValidHCL(hcl string) bool {
	r := regexp.MustCompile(`^#[\da-f]{6}$`)
	return r.Match([]byte(hcl))
}

func isValidECL(ecl string) bool {
	validECLs := map[string]bool{
		"amb": true,
		"blu": true,
		"brn": true,
		"gry": true,
		"grn": true,
		"hzl": true,
		"oth": true,
	}
	_, ok := validECLs[ecl]
	return ok
}
