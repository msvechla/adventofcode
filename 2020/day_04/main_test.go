package main

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_passport_isValid(t *testing.T) {
	tests := []struct {
		name string
		p    passport
		want bool
	}{
		{name: "1", p: passport{"cid": "147", "eyr": "2020", "ecl": "gry", "pid": "860033327", "hcl": "#fffffd", "byr": "1937", "iyr": "2017", "hgt": "183cm"}, want: true},
		{name: "2", p: passport{"ecl": "gry", "eyr": "2020", "pid": "860033327", "hcl": "#fffffd", "byr": "1937", "iyr": "2017", "hgt": "183cm"}, want: true},
		{name: "3", p: passport{"cid": "147", "eyr": "2020", "pid": "860033327", "hcl": "#fffffd", "byr": "1937", "iyr": "2017", "hgt": "183cm"}, want: false},
		{name: "4", p: passport{"pid": "860033327", "eyr": "2020", "hcl": "#fffffd", "byr": "1937", "iyr": "2017", "hgt": "183cm"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.isValid(); got != tt.want {
				t.Errorf("passport.isValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidNumber(t *testing.T) {
	type args struct {
		year   string
		length int
		min    int
		max    int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "byrValid", args: args{year: "2002", length: 4, min: 1920, max: 2002}, want: true},
		{name: "byrInvalid", args: args{year: "2003", length: 4, min: 1920, max: 2002}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidNumber(tt.args.year, tt.args.length, tt.args.min, tt.args.max); got != tt.want {
				t.Errorf("isValidYear() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidHGT(t *testing.T) {
	type args struct {
		hgt string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "validIN", args: args{hgt: "60in"}, want: true},
		{name: "validCM", args: args{hgt: "190cm"}, want: true},
		{name: "invalidIn", args: args{hgt: "190in"}, want: false},
		{name: "invalidNoUnit", args: args{hgt: "190"}, want: false},
		{name: "invalidNoVal", args: args{hgt: "cm"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidHGT(tt.args.hgt); got != tt.want {
				t.Errorf("isValidHeight() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidHCL(t *testing.T) {
	type args struct {
		hcl string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "1", args: args{hcl: "#123abc"}, want: true},
		{name: "2", args: args{hcl: "123abz"}, want: false},
		{name: "3", args: args{hcl: "123abc"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidHCL(tt.args.hcl); got != tt.want {
				t.Errorf("isValidHCL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidECL(t *testing.T) {
	type args struct {
		ecl string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "valid", args: args{ecl: "brn"}, want: true},
		{name: "invalid", args: args{ecl: "wat"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidECL(tt.args.ecl); got != tt.want {
				t.Errorf("isValidECL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parsePassports(t *testing.T) {
	tests := []struct {
		name string
		want passport
	}{
		{name: "1", want: passport{"eyr": "2020", "ecl": "hzl", "pid": "157096267", "byr": "1971", "iyr": "2017", "hgt": "160cm"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parsePassports(); !reflect.DeepEqual(got, tt.want) {
				assert.Equal(t, got[0], tt.want, tt.name)
			}
		})
	}
}
