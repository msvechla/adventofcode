# Advent of Code 2020 - Day 4: Passport Processing

- Puzzle: <https://adventofcode.com/2020/day/4>
- Code: [main.go](main.go)
- Tests: [main_test.go](main_test.go)

Run the Code:

```bash
go run main.go
```

